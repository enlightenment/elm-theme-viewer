#include <Elementary.h>
#include "common.h"

extern Gui_Data *gd;
extern Widget_Option_Data *wod;
Ecore_Timer *widget_timer = NULL;

Widget widgets[WIDGET_COUNT] = {
   { "none", ETV_ID_NONE, NULL },
   { "access", ETV_ID_ACCESS, NULL },
   { "actionslider", ETV_ID_ACTIONSLIDER, "An actionslider is a switcher for 2 or 3 labels with customizable magnet properties. The user drags and releases the indicator, to choose a label." },
   { "bg", ETV_ID_BG, "Background object, used for setting a solid color, image or Edje group as a background to a window or any container object." },
   { "border", ETV_ID_BORDER, NULL },
   { "bubble", ETV_ID_BUBBLE, "The Bubble is a widget to show text similar to how speech is represented in comics." },
   { "button", ETV_ID_BUTTON, "This is a push-button. Press it and run some function. It can contain a simple label and icon object and it also has an autorepeat feature." },
   { "calendar", ETV_ID_CALENDAR, "This is a calendar widget. It helps applications to flexibly display a calender with day of the week, date, year and month. Applications are able to set specific dates to be reported back, when selected, in the smart callbacks of the calendar widget." },
   { "check", ETV_ID_CHECK, "The check widget allows for toggling a value between true and false." },
   { "clock", ETV_ID_CLOCK, "This is a digital clock widget. In its default theme, it has a vintage 'flipping numbers clock' appearance, which will animate sheets of individual algarisms individually as time goes by." },
   { "colorselector", ETV_ID_COLORSELECTOR, "A ColorSelector is a color selection widget. It allows application to set a series of colors.It also allows to load/save colors from/to config with a unique identifier, by default, the colors are loaded/saved from/to config using 'default' identifier. The colors can be picked by user from the color set by clicking on individual color item on the palette or by selecting it from selector." },
   { "conformant", ETV_ID_CONFORMANT, NULL },
   { "ctxpopup", ETV_ID_CTXPOPUP, "A ctxpopup is a widget that, when shown, pops up a list of items. It automatically chooses an area inside its parent object's view (set via elm_ctxpopup_add() and elm_ctxpopup_hover_parent_set()) to optimally fit into it. In the default theme, it will also point an arrow to it's top left position at the time one shows it. Ctxpopup items have a label and/or an icon. It is intended for a small number of items (hence the use of list, not genlist)." },
   { "datetime", ETV_ID_DATETIME, "Datetime widget is used to display and input date & time values. This widget displays date and time as per the system's locale settings (Date includes Day, Month & Year along with the defined separators and Time includes Hour, Minute & AM/PM fields. Separator for AM/PM field is ignored." },
   { "dayselector", ETV_ID_DAYSELECTOR, "Dayselector is a day selection widget. It displays all seven days of the week and allows the user to select multiple days." },
   { "diskselector", ETV_ID_DISKSELECTOR, "A diskselector is a kind of list widget. It scrolls horizontally, and can contain label and icon objects. Three items are displayed with the selected one in the middle." },
   { "entry", ETV_ID_ENTRY, "An entry is a convenience widget which shows a box that the user can enter text into. Entries by default don't scroll, so they grow to accommodate the entire text, resizing the parent window as needed. This can be changed with the elm_entry_scrollable_set() function." },
   { "ews", ETV_ID_EWS, NULL },
   { "fileselector", ETV_ID_FILESELECTOR, "A file selector is a widget that allows a user to navigate through a file system, reporting file selections back via its API." },
   { "fileselector_button", ETV_ID_FILESELECTOR_BUTTON, "Fileselector button is a button that, when clicked, creates an Elementary window (or inner window) with a file selector widget within. When a file is chosen, the (inner) window is closed and the button emits a signal having the selected file as it's event_info." },
   { "fileselector_entry", ETV_ID_FILESELECTOR_ENTRY, "Fileselector entry is an entry made to be filled with or display a file system path string. Besides the entry itself, the widget has a file selector button on its side, which will raise an internal file selector widget, when clicked, for path selection aided by file system navigation." },
   { "flipselector", ETV_ID_FLIPSELECTOR, "A flip selector is a widget to show a set of text items, one at a time, with the same sheet switching style as the clock widget, when one changes the current displaying sheet (thus, the 'flip' in the name)." },
   { "focus_highlight", ETV_ID_FOCUS_HIGHLIGHT, NULL },
   { "frame", ETV_ID_FRAME, "Frame is a widget that holds some content and has a title." },
   { "gengrid", ETV_ID_GENGRID, NULL },
   { "genlist", ETV_ID_GENLIST, NULL },
   { "hover", ETV_ID_HOVER, "A Hover object will hover over its parent object at the target location. Anything in the background will be given a darker coloring to indicate that the hover object is on top (at the default theme). When the hover is clicked it is dismissed(hidden), if the contents of the hover are clicked that doesn't cause the hover to be dismissed." },
   { "icon", ETV_ID_ICON, "An icon object is used to display standard icon images (\"delete\", \"edit\", \"arrows\", etc.) or images coming from a custom file (PNG, JPG, EDJE, etc.), on icon contexts." },
   { "index", ETV_ID_INDEX, "An index widget gives you an index for fast access to whichever group of other UI items one might have. It's a list of text items (usually letters, for alphabetically ordered access)." },
   { "label", ETV_ID_LABEL, "Widget to display text, with simple html-like markup." },
   { "layout", ETV_ID_LAYOUT, NULL },
   { "list", ETV_ID_LIST, "A list widget is a container whose children are displayed vertically or horizontally, in order, and can be selected. The list can accept only one or multiple item selections. Also has many modes of items displaying." },
   { "map", ETV_ID_MAP, "This is a widget specifically for displaying a map. It uses basically OpenStreetMap provider http://www.openstreetmap.org/, but custom providers can be added." },
   { "menu", ETV_ID_MENU, "A menu is a list of items displayed above its parent. When the menu is showing its parent is darkened. Each item can have a sub-menu. The menu object can be used to display a menu on a right click event, in a toolbar, anywhere." },
   { "multibuttonentry", ETV_ID_MULTIBUTTONENTRY, NULL },
   { "naviframe", ETV_ID_NAVIFRAME, NULL },
   { "notify", ETV_ID_NOTIFY, "Display a container in a particular region of the parent(top, bottom, etc). A timeout can be set to automatically hide the notify. This is so that, after an evas_object_show() on a notify object, if a timeout was set on it, it will automatically get hidden after that time." },
   { "panel", ETV_ID_PANEL, "A panel is a type of animated container that contains subobjects. It can be expanded or contracted by clicking the button on it's edge." },
   { "panes", ETV_ID_PANES, NULL },
   { "photo", ETV_ID_PHOTO, "The Elementary photo widget is intended for displaying a photo, for ex., a person's image (contact). Simple, yet with a very specific purpose. It has a decorative frame around the inner image itself, on the default theme. If and while no photo is set on it, it displays a person icon, indicating it's a photo placeholder." },
   { "photocam", ETV_ID_PHOTOCAM, "Photocam is a widget meant specifically for displaying high-resolution digital camera photos, giving speedy feedback (fast load), zooming and panning as well as fitting logic, all with low memory footprint. It is entirely focused on jpeg images, and takes advantage of properties of the jpeg format (via Evas loader features in the jpeg loader)." },
   { "player", ETV_ID_PLAYER, NULL },
   { "pointer", ETV_ID_POINTER, NULL },
   { "popup", ETV_ID_POPUP, "This widget is an enhancement of Notify. <br>In addition to Content area, there are two optional sections namely Title area and Action area. <br>Popup Widget displays its content with a particular orientation in the parent area. This orientation can be one among top, center, bottom, left, top-left, top-right, bottom-left and bottom-right. Content part of Popup can be an Evas Object set by application or it can be Text set by application or set of items containing an icon and/or text. The content/item-list can be removed using elm_object_content_set with second parameter passed as NULL." },
   { "progressbar", ETV_ID_PROGRESSBAR, "The progress bar is a widget for visually representing the progress status of a given job/task." },
   { "radio", ETV_ID_RADIO, "Radio is a widget that allows for 1 or more options to be displayed and have the user choose only 1 of them." },
   { "scroller", ETV_ID_SCROLLER, NULL },
   { "segment_control", ETV_ID_SEGMENT_CONTROL, "Segment control widget is a horizontal control made of multiple segment items, each segment item functioning similar to discrete two state button. A segment control groups the items together and provides compact single button with multiple equal size segments." },
   { "separator", ETV_ID_SEPARATOR, "Separator is a very thin object used to separate other objects." },
   { "slider", ETV_ID_SLIDER, "The slider adds a draggable “slider” widget for selecting the value of something within a range." },
   { "slideshow", ETV_ID_SLIDESHOW, "This widget, as the name indicates, is a pre-made image slideshow panel, with API functions acting on (child) image items presentation. " },
   { "spinner", ETV_ID_SPINNER, "A spinner is a widget which allows the user to increase or decrease numeric values using arrow buttons, or edit values directly, clicking over it and typing the new value." },
   { "thumb", ETV_ID_THUMB, "A thumbnail object is used for displaying the thumbnail of an image or video. You must have compiled Elementary with Ethumb_Client support. Also, Ethumb's DBus service must be present and auto-activated in order to have thumbnails generated. You must also have a session bus, not a system one." },
   { "toolbar", ETV_ID_TOOLBAR, "A toolbar is a widget that displays a list of items inside a box. It can be scrollable, show a menu with items that don't fit to toolbar size or even crop them." },
   { "tooltip", ETV_ID_TOOLTIP, "The Tooltip is an (internal, for now) smart object used to show a content in a frame on mouse hover of objects(or widgets), with tips/information about them." },
   { "video", ETV_ID_VIDEO, "Elementary comes with two object that help design application that need to display video." },
   { "win", ETV_ID_WIN, "The window class of Elementary. Contains functions to manipulate windows. The Evas engine used to render the window contents is specified in the system or user elementary config files (whichever is found last), and can be overridden with the ELM_ENGINE environment variable for testing. Engines that may be supported (depending on Evas and Ecore-Evas compilation setup and modules actually installed at runtime) are (listed in order of best supported and most likely to be complete and work to lowest quality)." },
   { "none", ETV_ID_LAST, NULL }
};

static const char *lbl[] = {
   "Elementary",
   "Evas",
   "Eina",
   "Edje",
   "Eet",
   "Ecore",
   "Efreet",
   "Edbus"
};

/* Remove "/default" from the end of string.
 *
 * arrow_left/default -> arrow_left (icon)
 * separator/default -> separator (menu)
 */
static void
_trim_end_default(const char *orig_style, char *style)
{
   if (!orig_style) return;
   strncpy(style, orig_style, strlen(orig_style));
   style[strlen(orig_style) - strlen("/default")] = '\0';
   //INF("%s %s", style, orig_style);
}

// widget not implemented
static Evas_Object *
_widget_not_implemented_create(Evas_Object *parent, Widget_Type widget)
{
   Evas_Object *o;
   char buf[PATH_MAX] = {0, };

   o = elm_label_add(parent);
   EXPAND(o); FILL(o);
   elm_label_line_wrap_set(o, ELM_WRAP_MIXED);
   sprintf(buf, "Sorry, %s widget sample is not implemented yet.",
           widgets[widget].name);
   elm_object_text_set(o, buf);
   evas_object_show(o);

   return o;
}

// actionslider
static Evas_Object *
_widget_actionslider_create(Evas_Object *parent, const char *style)
{
   Evas_Object *as;

   as = elm_actionslider_add(parent);
   EXPAND(as); ALIGN(as, EVAS_HINT_FILL, 0);
   if (style) elm_object_style_set(as, style);
   elm_actionslider_enabled_pos_set(as, ELM_ACTIONSLIDER_CENTER |
                                    ELM_ACTIONSLIDER_RIGHT);
   elm_object_part_text_set(as, "left", NULL);
   elm_object_part_text_set(as, "center", "Accept");
   elm_object_part_text_set(as, "right", "Reject");
   if (wod)
     {
        int flag = 0;
        if (wod->actslider_magnet_pos_left) flag += 1;
        if (wod->actslider_magnet_pos_center) flag += 2;
        if (wod->actslider_magnet_pos_right) flag += 4;
        elm_actionslider_magnet_pos_set(as, flag);
        elm_actionslider_indicator_pos_set(as, (1 << wod->actslider_indi_pos));
     }
   else
     {
        elm_actionslider_magnet_pos_set(as, ELM_ACTIONSLIDER_CENTER|
                                        ELM_ACTIONSLIDER_RIGHT);
        elm_actionslider_indicator_pos_set(as, ELM_ACTIONSLIDER_LEFT);
     }
   evas_object_show(as);

   return as;
}

// access
static Evas_Object *
_widget_access_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o;

   elm_config_access_set(EINA_TRUE);

   o = elm_button_add(parent);
   EXPAND(o); ALIGN(o, EVAS_HINT_FILL, 0);
   elm_object_style_set(o, style);
   elm_object_text_set(o, "THIS IS A BUTTON TEST");
   elm_object_access_info_set(o, "this is a button test");
   evas_object_show(o);

   return o;
}

// bg
static Evas_Object *
_widget_bg_create(Evas_Object *parent, const char *style)
{
   Evas_Object *bg;
   bg = elm_bg_add(parent);
   EXPAND(bg); FILL(bg);
   if (style) elm_object_style_set(bg, style);
   evas_object_show(bg);
   return bg;
}

// border
static Evas_Object *
_widget_border_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o, *table;

   o = elm_layout_add(parent);
   elm_layout_theme_set(o, "border", "base", style);
   elm_layout_text_set(o, "elm.text.title", "Title");
   evas_object_show(o);

   table = util_elm_min_set(o, WIDGET_DEFAULT_WIDTH, WIDGET_DEFAULT_HEIGHT);
   ALIGN(table, 0.0, 0.0);

   return table;
}

// bubble
static Evas_Object *
_widget_bubble_create(Evas_Object *parent, const char *orig_style)
{
   char buf[PATH_MAX];
   char style[PATH_MAX] = {0, };
   Evas_Object *o, *ic, *lbl;

   ic = elm_icon_add(parent);
   snprintf(buf, sizeof(buf), "%s/images/sky_01.jpg", elm_app_data_dir_get());
   elm_image_file_set(ic, buf, NULL);
   evas_object_show(ic);

   lbl = elm_label_add(parent);
   elm_object_text_set(lbl, "This is a bubble widget preview.");
   evas_object_show(lbl);

   o = elm_bubble_add(parent);
   elm_object_text_set(o, "Title");
   elm_object_part_text_set(o, "info", "Information");
   elm_object_content_set(o, lbl);
   elm_object_part_content_set(o, "icon", ic);
   evas_object_show(o);

   _trim_end_default(orig_style, style);
   if (!strcmp("bottom_left", style))
     elm_bubble_pos_set(o, ELM_BUBBLE_POS_BOTTOM_LEFT);
   else if (!strcmp("bottom_right", style))
     elm_bubble_pos_set(o, ELM_BUBBLE_POS_BOTTOM_RIGHT);
   else if (!strcmp("top_left", style))
     elm_bubble_pos_set(o, ELM_BUBBLE_POS_TOP_LEFT);
   else if (!strcmp("top_right", style))
     elm_bubble_pos_set(o, ELM_BUBBLE_POS_TOP_RIGHT);

   return o;
}

// button
static Evas_Object *
_widget_button_create(Evas_Object *parent, const char* style)
{
   Evas_Object *o = NULL;
   o = elm_button_add(parent);
   EXPAND(o); ALIGN(o, EVAS_HINT_FILL, 0);
   evas_object_show(o);

   // strip out starting "base/" string.
   elm_object_style_set(o, style + strlen("base/"));

   if (wod)
     {
        if (wod->btn_text)
          elm_object_text_set(o, "This is a Button");
        if (wod->btn_image)
          {
             char buf[PATH_MAX] = { 0 };
             Evas_Object *icon = elm_icon_add(o);
             snprintf(buf, sizeof(buf), "%s/images/logo_small.png",
                      elm_app_data_dir_get());
             elm_image_file_set(icon, buf, NULL);
             evas_object_size_hint_aspect_set(icon,
                                              EVAS_ASPECT_CONTROL_VERTICAL,
                                              1, 1);
             evas_object_show(icon);
             elm_object_content_set(o, icon);
          }
     }
   else
     {
        elm_object_text_set(o, "This is a Button.");
     }

   return o;
}

// caclendar
static Evas_Object *
_widget_calendar_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o = NULL;

   o = elm_calendar_add(parent);
   EXPAND(o); FILL(o);
   evas_object_show(o);

   elm_object_style_set(o, style);

   return o;
}

// check
static Evas_Object *
_widget_check_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o;
   o = elm_check_add(parent);
   EXPAND(o);
   if (style) elm_object_style_set(o, style);
   evas_object_show(o);

   if (wod)
     {
        if (wod->chk_selected)
          elm_check_state_set(o, EINA_TRUE);
        if (wod->chk_text)
          elm_object_text_set(o, "Check Button");
        if (wod->chk_image)
          {
             char buf[PATH_MAX] = { 0 };
             Evas_Object *icon = elm_icon_add(o);
             snprintf(buf, sizeof(buf), "%s/images/logo_small.png",
                      elm_app_data_dir_get());
             elm_image_file_set(icon, buf, NULL);
             evas_object_size_hint_aspect_set(icon, EVAS_ASPECT_CONTROL_VERTICAL,
                                              1, 1);
             evas_object_show(icon);
             elm_object_content_set(o, icon);
          }

     }
   else
     {
        elm_object_text_set(o, "Check Button");
     }

   return o;
}

// clock
static Evas_Object *
_widget_clock_create(Evas_Object *parent, const char* style)
{
   Evas_Object *o;
   o = elm_clock_add(parent);
   EXPAND(o);
   elm_object_style_set(o, style);
   evas_object_show(o);

   return o;
}

// colorselector
static Evas_Object *
_widget_colorselector_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o;

   o = elm_colorselector_add(parent);
   EXPAND(o); FILL(o);
   elm_object_style_set(o, style);
   elm_colorselector_color_set(o, 255, 160, 132, 180);

   elm_colorselector_palette_color_add(o, 255, 90, 18, 255);
   elm_colorselector_palette_color_add(o, 255, 213, 0, 255);
   elm_colorselector_palette_color_add(o, 146, 255, 11, 255);
   elm_colorselector_palette_color_add(o, 9, 186, 10, 255);
   elm_colorselector_palette_color_add(o, 86, 201, 242, 255);
   elm_colorselector_palette_color_add(o, 18, 83, 128, 255);
   elm_colorselector_palette_color_add(o, 140, 53, 238, 255);
   elm_colorselector_palette_color_add(o, 255, 145, 145, 255);
   elm_colorselector_palette_color_add(o, 255, 59, 119, 255);
   elm_colorselector_palette_color_add(o, 133, 100, 69, 255);
   elm_colorselector_palette_color_add(o, 255, 255, 119, 255);
   elm_colorselector_palette_color_add(o, 133, 100, 255, 255);

   evas_object_show(o);

   return o;
}

// ctxpopup
static Evas_Object *ctxpopup = NULL; // singleton
static void
_widget_ctxpopup_dismissed_cb(void *data EINA_UNUSED,
                              Evas_Object *obj EINA_UNUSED,
                              void *event_info EINA_UNUSED)
{
   evas_object_del(ctxpopup);
   ctxpopup = NULL;
}

static void
_widget_ctxpopup_del_request(void *data EINA_UNUSED, Evas *e EINA_UNUSED,
                             Evas_Object *obj EINA_UNUSED,
                             void *event_info EINA_UNUSED)
{
   evas_object_del(ctxpopup);
   ctxpopup = NULL;
}

static void
_widget_ctxpopup_launch(void *data, Evas *e EINA_UNUSED,
                        Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Evas_Event_Mouse_Down *ev = event_info;
   Evas_Object *o = NULL;

   if (ctxpopup) evas_object_del(ctxpopup);

   ctxpopup = o = elm_ctxpopup_add(gd->win);
   elm_object_style_set(o, data);
   evas_object_smart_callback_add(o, "dismissed",
                                  _widget_ctxpopup_dismissed_cb, NULL);
   elm_ctxpopup_item_append(o, "Ctxpopup #1", NULL, _widget_ctxpopup_dismissed_cb, NULL);
   elm_ctxpopup_item_append(o, "Ctxpopup #2", NULL, _widget_ctxpopup_dismissed_cb, NULL);
   elm_ctxpopup_item_append(o, "Ctxpopup #3", NULL, _widget_ctxpopup_dismissed_cb, NULL);
   elm_ctxpopup_item_append(o, "Ctxpopup #4", NULL, _widget_ctxpopup_dismissed_cb, NULL);
   elm_ctxpopup_item_append(o, "Ctxpopup #5", NULL, _widget_ctxpopup_dismissed_cb, NULL);

   evas_object_move(o, ev->canvas.x, ev->canvas.y);
   evas_object_show(o);
}

static Evas_Object *
_widget_ctxpopup_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o = NULL;

   o = elm_label_add(parent);
   EXPAND(o); FILL(o);
   elm_object_text_set(o, "Click here to create a ctxpopup");
   evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_DOWN,
                                  _widget_ctxpopup_launch, style);
   evas_object_event_callback_add(o, EVAS_CALLBACK_DEL,
                                  _widget_ctxpopup_del_request, NULL);
   evas_object_show(o);

   return o;
}

// datetime
static Evas_Object *
_widget_datetime_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o = NULL;

   o = elm_datetime_add(parent);
   EXPAND(o); FILL(o);
   elm_object_style_set(o, style);

   if (wod)
     {
        elm_datetime_field_visible_set(o, ELM_DATETIME_YEAR, wod->dt_year);
        elm_datetime_field_visible_set(o, ELM_DATETIME_MONTH, wod->dt_month);
        elm_datetime_field_visible_set(o, ELM_DATETIME_DATE, wod->dt_date);
        elm_datetime_field_visible_set(o, ELM_DATETIME_HOUR, wod->dt_hour);
        elm_datetime_field_visible_set(o, ELM_DATETIME_MINUTE, wod->dt_minute);
        elm_datetime_field_visible_set(o, ELM_DATETIME_AMPM, wod->dt_ampm);
     }

   evas_object_show(o);

   return o;
}

// entry
static Evas_Object *
_widget_entry_create(Evas_Object *parent, const char *orig_style, const char* style)
{
   char buf[PATH_MAX];
   char type[PATH_MAX];
   Evas_Object *o;

   _style_split_1(orig_style, type);

   o = elm_entry_add(parent);
   EXPAND(o); FILL(o);

   if (!strcmp("emoticon", type))
     {
        const char *emoticon_style = NULL;
        emoticon_style = _style_split_2(orig_style);
        snprintf(buf, sizeof(buf), "<item size=64x64 href=emoticon/%s></item>", emoticon_style);
     }
   else
     {
        elm_object_style_set(o, style);

        if (wod)
          {
             switch (wod->entry_text_len)
               {
                case 0:
                   snprintf(buf, sizeof(buf), "style : %s\n", style);
                   break;
                case 1:
                   snprintf(buf, sizeof(buf), "This is entry test. style : %s\n", style);
                   break;
                case 2:
                default:
                   snprintf(buf, sizeof(buf),
                            "This is an entry widget.<br/>"
                            "Entry uses markup <b>like this</> for styling.<br/>"
                            "This is %s style.<br/><br/>"
                            "[line wrap test]<br/>"
                            "ASDFASDFWWWW aslkdfjaslkdjf wioejrlkajsdflj asldkfj asldfkjasldfkjlsakdjf alskdjfalsdkfjsaldkfjaslkfj"
                            , style);
                   break;
               }

             if (wod->entry_guide_text)
               elm_object_part_text_set(o, "guide", "Type in here.");
          }
        else
          {
             snprintf(buf, sizeof(buf),
                      "This is an entry widget.<br/>"
                      "Entry uses markup <b>like this</> for styling.<br/>"
                      "This is %s style.<br/><br/>"
                      "[line wrap test]<br/>"
                      "ASDFASDFWWWW aslkdfjaslkdjf wioejrlkajsdflj asldkfj asldfkjasldfkjlsakdjf alskdjfalsdkfjsaldkfjaslkfj"
                      , style);
          }
     }

   // use entry's features
   // TODO: anchor, cursor, magnifier, path/separator, selection
   if (!strcmp("base-password", type))
     {
        elm_entry_password_set(o, EINA_TRUE);
        strncpy(buf, "1234", 5);
     }
   else if (!strcmp("base-single", type))
     elm_entry_single_line_set(o, EINA_TRUE);
   else if (!strcmp("base-charwrap", type))
     elm_entry_line_wrap_set(o, ELM_WRAP_CHAR);
   else if (!strcmp("base-mixedwrap", type))
     elm_entry_line_wrap_set(o, ELM_WRAP_MIXED);
   else if (!strcmp("base-nowrap", type))
     elm_entry_line_wrap_set(o, ELM_WRAP_NONE);
   else if (!strcmp("base-single-noedit", type))
     {
        elm_entry_editable_set(o, EINA_FALSE);
        elm_entry_single_line_set(o, EINA_TRUE);
     }
   else if (!strcmp("base-noedit-charwrap", type))
     {
        elm_entry_editable_set(o, EINA_FALSE);
        elm_entry_line_wrap_set(o, ELM_WRAP_CHAR);
     }
   else if (!strcmp("base-noedit", type))
     {
        elm_entry_editable_set(o, EINA_FALSE);
        elm_entry_line_wrap_set(o, ELM_WRAP_WORD);
     }
   else if (!strcmp("base-noedit-mixedwrap", type))
     {
        elm_entry_editable_set(o, EINA_FALSE);
        elm_entry_line_wrap_set(o, ELM_WRAP_MIXED);
     }
   else if (!strcmp("base-nowrap-noedit", type))
     {
        elm_entry_editable_set(o, EINA_FALSE);
        elm_entry_line_wrap_set(o, ELM_WRAP_NONE);
     }

   elm_object_text_set(o, buf);
   evas_object_show(o);

   return o;
}

// ews
static Evas_Object *
_widget_ews_create(Evas_Object *parent, const char *orig_style)
{
   Evas_Object *o, *table, *edje;
   const char *group = NULL, *style = NULL;
   char buf[PATH_MAX] = {0, };

   strncpy(buf, orig_style, sizeof(buf) - 1);
   group = strtok(buf, "/");
   style = strtok(NULL, "/");

   o = elm_layout_add(parent);
   elm_layout_theme_set(o, "ews", group, style);
   evas_object_show(o);

   edje = elm_layout_edje_get(o);
   if (edje_object_part_exists(edje, "elm.text.title"))
     elm_layout_text_set(o, "elm.text.title", "Title");

   table = util_elm_min_set(o, WIDGET_DEFAULT_WIDTH, WIDGET_DEFAULT_HEIGHT);
   ALIGN(table, 0.0, 0.0);

   return table;
}

// frame
static Evas_Object *
_widget_frame_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o, *lbl;
   char buf[PATH_MAX];

   o = elm_frame_add(parent);
   elm_object_style_set(o, style);
   EXPAND(o); ALIGN(o, EVAS_HINT_FILL, 0);
   elm_object_text_set(o, "Frame Styles");
   evas_object_show(o);

   lbl = elm_label_add(parent);
   sprintf(buf, "This is a %s style frame.", style);
   elm_label_line_wrap_set(lbl, ELM_WRAP_MIXED);
   elm_object_text_set(lbl, buf);
   elm_object_content_set(o, lbl);
   evas_object_show(lbl);

   return o;
}

// gengrid
static char *
_widget_gengrid_text_get(void *data, Evas_Object *obj EINA_UNUSED,
                         const char *part EINA_UNUSED)
{
   char buf[256];

   if (wod)
     {
        switch (wod->gengrid_text_len)
          {
           case 0:
              snprintf(buf, sizeof(buf),
                       "Item # %i",
                       (int)(long)data);
              break;
           case 1:
              snprintf(buf, sizeof(buf),
                       "Item # %i - Gengrid item test!",
                       (int)(long)data);
              break;
           case 2:
           default:
              snprintf(buf, sizeof(buf),
                       "Item # %i - elm-theme-viewer is an awesome program! Feel the new horizon.",
                       (int)(long)data);
              break;
          }
     }
   else
     {
        snprintf(buf, sizeof(buf),
                 "Item # %i - elm-theme-viewer is an awesome program! Feel the new horizon.",
                 (int)(long)data);
     }

   return strdup(buf);
}

static Evas_Object *
_widget_gengrid_grid_check_content_get(void *data, Evas_Object *obj,
                                       const char *part)
{
   Evas_Object *o = NULL;
   char buf[PATH_MAX] = {0, };
   int i = ((int)(long)data % 4) + 1;

   if (!strcmp(part, "elm.swallow.icon"))
     {
        o = elm_bg_add(obj);
        snprintf(buf, sizeof(buf),
                 "%s/images/sky_0%d.jpg", elm_app_data_dir_get(), i);

        elm_bg_file_set(o, buf, NULL);
        evas_object_show(o);
     }
   else if (!strcmp(part, "elm.swallow.end"))
     {
        o = elm_check_add(obj);
        elm_object_style_set(o, "grid");
        evas_object_propagate_events_set(o, EINA_FALSE);
        evas_object_show(o);
     }
   return o;
}

static Evas_Object *
_widget_gengrid_content_get(void *data, Evas_Object *obj, const char *part)
{
   Evas_Object *o = NULL;
   char buf[PATH_MAX] = {0, };
   int i = ((int)(long)data % 4) + 1;

   if (!strcmp(part, "elm.swallow.icon"))
     {
        o = elm_bg_add(obj);
        snprintf(buf, sizeof(buf),
                 "%s/images/sky_0%d.jpg", elm_app_data_dir_get(), i);

        elm_bg_file_set(o, buf, NULL);
        evas_object_show(o);
     }
   else if (!strcmp(part, "elm.swallow.end"))
     {
        o = elm_check_add(obj);
        evas_object_propagate_events_set(o, EINA_FALSE);
        evas_object_show(o);
     }
   return o;
}

static void
_gengrid_item_sel_cb(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED,
                     void *event_info)
{
   printf("Item %p is selected\n", event_info);
}

static Evas_Object *
_widget_gengrid_icon_aspect_content_get(void *data, Evas_Object *obj,
                                        const char *part)
{
   Evas_Object *o = NULL;
   char buf[PATH_MAX] = {0, };
   int i = ((int)(long)data % 4) + 1;

   if (!strcmp(part, "elm.swallow.icon"))
     {
        o = elm_bg_add(obj);
        snprintf(buf, sizeof(buf),
                 "%s/images/sky_0%d.jpg", elm_app_data_dir_get(), i);

        elm_bg_file_set(o, buf, NULL);
        evas_object_size_hint_aspect_set(o, EVAS_ASPECT_CONTROL_VERTICAL,
                                         1, 1);
        evas_object_show(o);
     }
   else if (!strcmp(part, "elm.swallow.end"))
     {
        o = elm_check_add(obj);
        evas_object_propagate_events_set(o, EINA_FALSE);
        evas_object_show(o);
     }
   return o;
}

typedef enum {
   GENGRID_NORMAL_STYLE,
   GENGRID_GROUP_INDEX_STYLE,
   GENGRID_DEFAULT_STYLE_STYLE,
   GENGRID_H9_GRID_CHECK_STYLE,
   GENGRID_H9_GROUP_INDEX_STYLE
} Gengrid_Style_Type;

static Evas_Object *
_widget_gengrid_create(Evas_Object *parent, const char *orig_style, const char *style)
{
   Evas_Object *o;
   Elm_Gengrid_Item_Class *ic = NULL, *group_ic = NULL;
   int i = 0;
   char buf[PATH_MAX] = {0, };
   Gengrid_Style_Type gst = GENGRID_NORMAL_STYLE;

   /* classify styles */
   if (style && !strcmp("group_index", style))
     gst = GENGRID_GROUP_INDEX_STYLE;
   else if (style && !strcmp("default_style", style))
     gst = GENGRID_DEFAULT_STYLE_STYLE;
   else if (!strcmp("h9 grid-check-style", orig_style))
     gst = GENGRID_H9_GRID_CHECK_STYLE;
   else if (!strcmp("h9 group-index-style", orig_style))
     gst = GENGRID_H9_GROUP_INDEX_STYLE;

   o = elm_gengrid_add(parent);
   elm_gengrid_align_set(o, 0, 0);
   if (wod)
     {
        elm_gengrid_item_size_set(o,
                                  elm_config_scale_get() * wod->gengrid_item_width,
                                  elm_config_scale_get() * wod->gengrid_item_height);
        elm_gengrid_align_set(o,
                              wod->gengrid_horz_align,
                              wod->gengrid_vert_align);
     }
   else
     {
        elm_gengrid_item_size_set(o,
                                  elm_config_scale_get() * 168, elm_config_scale_get() * 168);
        elm_gengrid_align_set(o, 0.0, 0.0);
     }
   EXPAND(o); FILL(o);
   evas_object_show(o);

   ic = elm_gengrid_item_class_new();
   ic->func.text_get = _widget_gengrid_text_get;
   ic->func.state_get = NULL;
   ic->func.del = NULL;

   /* check for gengrid style or gengrid item style */
   strncpy(buf, orig_style, sizeof(buf) - 1);
   if ((gst == GENGRID_H9_GRID_CHECK_STYLE) ||
       (gst == GENGRID_GROUP_INDEX_STYLE) ||
       (gst == GENGRID_H9_GROUP_INDEX_STYLE))
     ic->item_style = "default";
   else if (!strncmp("item", strtok(buf, "/"), 4))
     ic->item_style = style;
   else
     elm_object_style_set(o, style);

   /* check for special custom style for h9 grid-check-style */
   if (gst == GENGRID_H9_GRID_CHECK_STYLE)
     ic->func.content_get = _widget_gengrid_grid_check_content_get;
   else if ((gst == GENGRID_GROUP_INDEX_STYLE) ||
            (gst == GENGRID_DEFAULT_STYLE_STYLE))
     ic->func.content_get = _widget_gengrid_icon_aspect_content_get;
   else
     ic->func.content_get = _widget_gengrid_content_get;

   /* check for group index */
   if (gst == GENGRID_H9_GROUP_INDEX_STYLE)
     elm_gengrid_group_item_size_set(o, 102, 102);
   else if (gst == GENGRID_GROUP_INDEX_STYLE)
     elm_gengrid_group_item_size_set(o, 31, 31);

   /* apply options */
   if (wod)
     elm_gengrid_select_mode_set(o, wod->gengrid_select_mode);

   if ((gst == GENGRID_H9_GROUP_INDEX_STYLE) ||
       (gst == GENGRID_GROUP_INDEX_STYLE))
     {
        group_ic = elm_gengrid_item_class_new();
        group_ic->item_style = "group_index";
        group_ic->func.text_get = _widget_gengrid_text_get;
        group_ic->func.content_get = NULL;
        group_ic->func.state_get = NULL;
        group_ic->func.del = NULL;
     }

   for (i = 0; i < 50; i++)
     {
        if (((i % 10) == 0) &&
            ((gst == GENGRID_H9_GROUP_INDEX_STYLE) ||
             (gst == GENGRID_GROUP_INDEX_STYLE)))
          elm_gengrid_item_append(o, group_ic, (void *)(long)i,
                                  _gengrid_item_sel_cb, NULL);
        elm_gengrid_item_append(o, ic, (void *)(long)i,
                                _gengrid_item_sel_cb, NULL);
     }

   if ((gst == GENGRID_H9_GROUP_INDEX_STYLE) ||
       (gst == GENGRID_GROUP_INDEX_STYLE))
     elm_gengrid_item_class_free(group_ic);

   elm_gengrid_item_class_free(ic);

   return o;
}

// genlist
static char *
_widget_genlist_text_get(void *data, Evas_Object *obj EINA_UNUSED,
                         const char *part EINA_UNUSED)
{
   char buf[256];

   if (wod)
     {
        switch (wod->genlist_text_len)
          {
           case 0:
              snprintf(buf, sizeof(buf),
                       "Item # %i",
                       (int)(long)data);
              break;
           case 1:
              snprintf(buf, sizeof(buf),
                       "Item # %i - Genlist item test!",
                       (int)(long)data);
              break;
           case 2:
           default:
              snprintf(buf, sizeof(buf),
                       "Item # %i - elm-theme-viewer is an awesome program! Feel the new horizon.",
                       (int)(long)data);
              break;
          }
     }
   else
     {
        snprintf(buf, sizeof(buf),
                 "Item # %i - elm-theme-viewer is an awesome program! Feel the new horizon.",
                 (int)(long)data);
     }

   return strdup(buf);
}

static Evas_Object *
_widget_genlist_content_get(void *data EINA_UNUSED, Evas_Object *obj,
                            const char *part)
{
   Evas_Object *o = NULL;

   if (wod)
     {
        switch (wod->genlist_content_type)
          {
           case 0:
              o = elm_icon_add(obj);
              if (!strcmp(part, "elm.swallow.end"))
                elm_icon_standard_set(o, "folder");
              else
                elm_icon_standard_set(o, "home");
              evas_object_size_hint_aspect_set(o, EVAS_ASPECT_CONTROL_VERTICAL, 1, 1);
              break;
           case 1:
              o = elm_check_add(obj);
              break;
           case 2:
           default:
              o = elm_button_add(obj);
              elm_object_text_set(o, "Button");
              break;
          }
     }
   else
     {
        o = elm_icon_add(obj);
        if (!strcmp(part, "elm.swallow.end"))
          elm_icon_standard_set(o, "folder");
        else
          elm_icon_standard_set(o, "home");
        evas_object_size_hint_aspect_set(o, EVAS_ASPECT_CONTROL_VERTICAL, 1, 1);
     }
   return o;
}

static void
_genlist_item_sel_cb(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED,
                     void *event_info)
{
   printf("Item %p is selected\n", event_info);
}

static Evas_Object *
_widget_genlist_create(Evas_Object *parent, const char *orig_style, const char *style)
{
   Evas_Object *o;
   Elm_Genlist_Item_Class *ic;
   int i = 0;
   char buf[PATH_MAX] = {0, };
   const char *token;
   Elm_Genlist_Item_Type item_type = ELM_GENLIST_ITEM_NONE;

   o = elm_genlist_add(parent);
   EXPAND(o); FILL(o);
   evas_object_show(o);

   ic = elm_genlist_item_class_new();
   ic->func.text_get = _widget_genlist_text_get;
   ic->func.content_get = _widget_genlist_content_get;
   ic->func.state_get = NULL;
   ic->func.del = NULL;

   strncpy(buf, orig_style, sizeof(buf) - 1);
   token = strtok(buf, "/");

   /* set item or genlist style */
   if (!strncmp("item", token, 4) || !strncmp("tree", token, 4))
     ic->item_style = style;
   else
     elm_object_style_set(o, style);

   /* check compress mode */
   if (!strncmp("item_compress", token, 13) ||
       !strncmp("tree_compress", token, 13))
     elm_genlist_mode_set(o, ELM_LIST_COMPRESS);

   /* apply options */
   if (wod)
     elm_genlist_select_mode_set(o, wod->genlist_select_mode);

   /* check tree */
   if (!strncmp("tree", token, 4))
     item_type = ELM_GENLIST_ITEM_TREE;

   for (i = 0; i < 50; i++)
     {
        elm_genlist_item_append(o, ic, (void *)(long)i, NULL, item_type,
                                _genlist_item_sel_cb, NULL);
     }

   elm_genlist_item_class_free(ic);

   return o;
}

// fileselector
static Evas_Object *
_widget_fileselector_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o;
   char buf[40];

   o = elm_fileselector_add(parent);
   EXPAND(o); FILL(o);
   elm_fileselector_is_save_set(o, EINA_TRUE);
   elm_fileselector_expandable_set(o, EINA_FALSE);
   elm_fileselector_path_set(o, getenv("HOME"));
   EXPAND(o); FILL(o);
   sprintf(buf, "%s Style.", style);
   elm_object_text_set(o, buf);
   evas_object_show(o);

   return o;
}

// fileselector entry
static Evas_Object *
_widget_fileselector_entry_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o, *ic;

   ic = elm_icon_add(parent);
   elm_icon_standard_set(ic, "file");
   evas_object_size_hint_aspect_set(ic, EVAS_ASPECT_CONTROL_VERTICAL, 1, 1);
   evas_object_show(ic);

   o = elm_fileselector_entry_add(parent);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_fileselector_path_set(o, getenv("HOME"));
   elm_object_style_set(o, style);
   elm_object_text_set(o, "Select a file");
   elm_object_part_content_set(o, "button icon", ic);
   evas_object_show(o);

   return o;
}

// flipselector
static Evas_Object *
_widget_flipselector_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o;
   unsigned int i = 0;

   o = elm_flipselector_add(parent);
   elm_object_style_set(o, style);
   WEIGHT(o, 0.0, 0.0); ALIGN(o, 0.5, 0.5);
   for (i = 0; i < sizeof(lbl) / sizeof(char *); i++)
     {
        elm_flipselector_item_append(o, lbl[i], NULL, NULL);
     }
   evas_object_show(o);

   return o;
}

// focus highlight
static void
_widget_focus_highlight_win_create(void *data, Evas_Object *obj EINA_UNUSED,
                                   void *event_info EINA_UNUSED)
{
   Evas_Object *fwin = NULL, *box0 = NULL, *box = NULL, *o;
   char style[PATH_MAX] = {0, };

   fwin =  elm_win_util_standard_add("focus-window", "Focus Window");
   elm_win_autodel_set(fwin, EINA_TRUE);

   strncpy(style, data + 4, strlen(data) - 3); // "top/"
   elm_object_theme_set(fwin, th);
   elm_win_focus_highlight_enabled_set(fwin, EINA_TRUE);

   box0 = o = elm_box_add(fwin);
   EXPAND(o);
   elm_win_resize_object_add(fwin, o);
   elm_box_padding_set(o, 100, 100);
   evas_object_show(o);

   box = o = elm_box_add(box0);
   EXPAND(o); FILL(o);
   elm_box_pack_end(box0, o);
   evas_object_show(o);

   o = elm_button_add(box);
   elm_object_focus_highlight_style_set(o, style);
   elm_object_text_set(o, "button");
   elm_box_pack_end(box, o);
   evas_object_show(o);

   o = elm_check_add(box);
   elm_object_focus_highlight_style_set(o, style);
   elm_object_text_set(o, "check");
   elm_box_pack_end(box, o);
   evas_object_show(o);

   evas_object_resize(fwin, 400, 400);
   evas_object_show(fwin);
}

static Evas_Object *
_widget_focus_highlight_create(Evas_Object *parent, const char *orig_style)
{
   Evas_Object *o = NULL;
   o = elm_button_add(parent);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_object_text_set(o, "Press");
   evas_object_smart_callback_add(o, "clicked",
                                  _widget_focus_highlight_win_create,
                                  orig_style);
   evas_object_show(o);

   return o;
}

static void
_hover_bt_cb(void *data, Evas_Object *obj EINA_UNUSED,
             void *event_info EINA_UNUSED)
{
   evas_object_show(data);
}

// hover
static Evas_Object *
_widget_hover_create(Evas_Object *parent, const char* style)
{
   Evas_Object *hv, *bt, *bt2;

   hv = elm_hover_add(parent);
   elm_object_style_set(hv, style);

   bt = elm_button_add(parent);
   EXPAND(bt); ALIGN(bt, EVAS_HINT_FILL, 0);
   elm_object_text_set(bt, "click here to see hover");
   evas_object_smart_callback_add(bt, "clicked", _hover_bt_cb, hv);
   elm_hover_parent_set(hv, gd->win);
   elm_hover_target_set(hv, bt);
   evas_object_show(bt);

   bt2 = elm_button_add(parent);
   elm_object_text_set(bt2, "Popup");
   elm_object_part_content_set(hv, "middle", bt2);
   evas_object_show(bt2);

   return bt;
}

// index
static Evas_Object *
_widget_index_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o;
   char buf[2] = {0, };
   int i = 0;

   o = elm_index_add(parent);
   EXPAND(o); FILL(o);

   if (!strcmp("horizontal", style))
     elm_index_horizontal_set(o, EINA_TRUE);

   for (i = 0; i < 26; i++)
     {
        snprintf(buf, sizeof(buf), "%c", 'A' + i);
        elm_index_item_append(o, buf, NULL, NULL);
     }
   elm_index_autohide_disabled_set(o, EINA_TRUE);
   elm_index_level_go(o, 0);
   evas_object_show(o);

   return o;
}

// icon
static Evas_Object *
_widget_icon_create(Evas_Object *parent, const char *orig_style)
{
   Evas_Object *o;
   char style[PATH_MAX] = {0, };

   _trim_end_default(orig_style, style);

   o = elm_icon_add(parent);
   EXPAND(o);
   elm_icon_standard_set(o, style);
   evas_object_size_hint_min_set(o, 100, 100);
   evas_object_show(o);

   return o;
}

// label
static Evas_Object *
_widget_label_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o;
   char buf[PATH_MAX] = {0, };

   o = elm_label_add(parent);
   EXPAND(o); FILL(o);
   sprintf(buf, "This is a %s style label.", style);
   elm_object_text_set(o, buf);
   elm_object_style_set(o, style);
   evas_object_show(o);

   return o;
}

// layout
static Evas_Object *
_widget_layout_content_create(Evas_Object *layout)
{
   Evas_Object *table, *bg, *lbl;

   table = elm_table_add(layout);
   EXPAND(table); FILL(table);
   evas_object_show(table);

   bg = elm_bg_add(table);
   EXPAND(bg); FILL(bg);
   elm_bg_color_set(bg, 100, 100, 100);
   elm_table_pack(table, bg, 0, 0, 1, 1);
   evas_object_show(bg);

   lbl = elm_label_add(table);
   EXPAND(lbl); FILL(lbl);
   elm_label_line_wrap_set(lbl, ELM_WRAP_MIXED);
   elm_object_text_set(lbl, "This dark area is a content area.");
   elm_table_pack(table, lbl, 0, 0, 1, 1);
   evas_object_show(lbl);

   return table;
}

static void
_widget_layout_icon_create(Evas_Object *layout)
{
   Evas_Object *ic;

   ic = elm_icon_add(layout);
   elm_icon_standard_set(ic, "chat");
   evas_object_size_hint_min_set(ic, 20, 20);
   elm_layout_icon_set(layout, ic);
}

static void
_widget_layout_end_create(Evas_Object *layout)
{
   Evas_Object *ic;

   ic = elm_icon_add(layout);
   elm_icon_standard_set(ic, "close");
   evas_object_size_hint_min_set(ic, 20, 20);
   elm_layout_end_set(layout, ic);
}

static Evas_Object *
_widget_layout_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o, *lbl, *content;

   o = elm_layout_add(parent);
   elm_layout_theme_set(o, "layout", "application", style);
   EXPAND(o); FILL(o);
   evas_object_show(o);

   if (!strcmp("toolbar-content", style))
     {
        content = _widget_layout_content_create(o);
        elm_layout_content_set(o, "elm.swallow.content", content);
     }
   else if (!strcmp("toolbar-content-back", style))
     {
        elm_object_part_text_set(o, "elm.text.title", "Layout Title");
        _widget_layout_end_create(o);
        content = _widget_layout_content_create(o);
        elm_layout_content_set(o, "elm.swallow.content", content);
     }
   else if (!strcmp("toolbar-content-back-next", style))
     {
        elm_object_part_text_set(o, "elm.text.title", "Layout Title");
        elm_object_part_text_set(o, "elm.text.title", "Layout Title");
        content = _widget_layout_content_create(o);
        elm_layout_content_set(o, "elm.swallow.content", content);
     }
   else if (!strcmp("content-back", style))
     {
        elm_object_part_text_set(o, "elm.text.title", "Layout Title");
        _widget_layout_end_create(o);
        content = _widget_layout_content_create(o);
        elm_layout_content_set(o, "elm.swallow.content", content);
     }
   else if (!strcmp("content-back-next", style))
     {
        elm_object_part_text_set(o, "elm.text.title", "Layout Title");
        content = _widget_layout_content_create(o);
        elm_layout_content_set(o, "elm.swallow.content", content);
     }
   else if (!strcmp("titlebar", style))
     {
        elm_object_part_text_set(o, "elm.text", "Layout Title");
        _widget_layout_icon_create(o);
        _widget_layout_end_create(o);
        content = _widget_layout_content_create(o);
        elm_layout_content_set(o, "elm.swallow.content", content);
     }
   else if (!strcmp("toolbar-table", style))
     {
        content = _widget_layout_content_create(o);
        elm_layout_table_pack(o, "elm.table.content", content, 0, 0, 1, 1);

        lbl = elm_label_add(parent);
        elm_label_line_wrap_set(lbl, ELM_WRAP_MIXED);
        elm_object_text_set(lbl, "This area is also a content area which packed into a table.");
        EXPAND(lbl); FILL(lbl);
        evas_object_show(lbl);
        elm_layout_table_pack(o, "elm.table.content", lbl, 0, 1, 1, 1);

        lbl = elm_label_add(parent);
        elm_label_line_wrap_set(lbl, ELM_WRAP_MIXED);
        elm_object_text_set(lbl, "This area is also a content area which packed into a table.");
        EXPAND(lbl); FILL(lbl);
        evas_object_show(lbl);
        elm_layout_table_pack(o, "elm.table.content", lbl, 1, 0, 1, 1);

        content = _widget_layout_content_create(o);
        elm_layout_table_pack(o, "elm.table.content", content, 1, 1, 1, 1);
     }
   else if (!strcmp("toolbar-vbox", style))
     {
        content = _widget_layout_content_create(o);
        elm_layout_box_append(o, "elm.box.content", content);

        lbl = elm_label_add(parent);
        elm_label_line_wrap_set(lbl, ELM_WRAP_MIXED);
        elm_object_text_set(lbl, "This area is also a content area which packed into a box.");
        EXPAND(lbl); FILL(lbl);
        evas_object_show(lbl);
        elm_layout_box_append(o, "elm.box.content", lbl);
     }

   return o;
}

// list
static Evas_Object *
_widget_list_create(Evas_Object *parent, const char *orig_style, const char *style)
{
   Evas_Object *o = NULL, *icon = NULL;
   int i = 0;
   char *buf = NULL;
   char type[PATH_MAX] = {0, };

   o = elm_list_add(parent);
   EXPAND(o); FILL(o);
   elm_object_style_set(o, style);
   evas_object_show(o);

   _style_split_1(orig_style, type);

   if (strstr(type, "h_item"))
     elm_list_horizontal_set(o, EINA_TRUE);

   if (strstr(type, "compress"))
     elm_list_mode_set(o, ELM_LIST_COMPRESS);

   for (i = 0; i < 50; i++)
     {
        icon = elm_icon_add(o);
        elm_icon_standard_set(icon, "folder");

        buf = calloc(20, sizeof(char));
        sprintf(buf, "list item # %d", i);
        elm_list_item_append(o, buf, icon, NULL, NULL, NULL);
        free(buf);
     }
   elm_list_go(o);

   return o;
}

// menu
static void
_widget_menu_resize(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj,
                    void *event EINA_UNUSED)
{
   Evas_Coord w, h;
   Evas_Object *bg = data;

   evas_object_geometry_get(obj, NULL, NULL, &w, &h);
   evas_object_resize(bg, w, h);
}

static void
_widget_menu_show(void *data, Evas *e EINA_UNUSED,
                  Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Evas_Event_Mouse_Down *ev = event_info;
   elm_menu_move(data, ev->canvas.x, ev->canvas.y);
   evas_object_show(data);
}

static Evas_Object *
_widget_menu_create(Evas_Object *parent, const char *orig_style)
{
   Evas_Object *o, *bg, *table, *lbl;
   Elm_Object_Item *menu_it;
   char style[PATH_MAX] = {0, };

   _trim_end_default(orig_style, style);

   table = elm_table_add(parent);
   EXPAND(table); FILL(table);
   evas_object_show(table);

   bg = elm_bg_add(parent);
   EXPAND(bg); FILL(bg);
   elm_bg_color_set(bg, 100, 100, 100);
   elm_table_pack(table, bg, 0, 0, 1, 1);
   evas_object_show(bg);

   lbl = elm_label_add(table);
   elm_label_line_wrap_set(lbl, ELM_WRAP_MIXED);
   EXPAND(lbl); FILL(lbl);
   elm_object_text_set(lbl, "Click this dark area.");
   elm_table_pack(table, lbl, 0, 0, 1, 1);
   evas_object_repeat_events_set(lbl, EINA_TRUE);
   evas_object_show(lbl);

   o = elm_menu_add(parent);
   elm_object_theme_set(o, th);
   if (!strcmp("item", style))
     {
        elm_menu_item_add(o, NULL, NULL, "first item", NULL, NULL);
        elm_menu_item_add(o, NULL, "mail-reply-all", "second item", NULL, NULL);
        elm_menu_item_add(o, NULL, "window-new", "third item", NULL, NULL);
     }
   else if (!strcmp("item_with_submenu", style))
     {
        elm_menu_item_add(o, NULL, NULL, "first item", NULL, NULL);
        menu_it = elm_menu_item_add(o, NULL, "mail-reply-all", "second item", NULL, NULL);
        elm_menu_item_add(o, menu_it, NULL, "first sub menu", NULL, NULL);
        elm_menu_item_add(o, menu_it, "mail-reply-all", "second sub menu", NULL, NULL);
     }
   else if (!strcmp("separator", style))
     {
        elm_menu_item_add(o, NULL, NULL, "between separator", NULL, NULL);
        elm_menu_item_separator_add(o, NULL);
        elm_menu_item_add(o, NULL, NULL, "between separator", NULL, NULL);
        elm_menu_item_separator_add(o, NULL);
        elm_menu_item_add(o, NULL, NULL, "between separator", NULL, NULL);
     }

   evas_object_event_callback_add(gd->win, EVAS_CALLBACK_RESIZE,
                                  _widget_menu_resize, bg);
   evas_object_event_callback_add(bg, EVAS_CALLBACK_MOUSE_DOWN,
                                  _widget_menu_show, o);

   return table;
}

// multibuttonentry
static Evas_Object *
_widget_multibuttonentry_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o = NULL;

   o = elm_multibuttonentry_add(parent);
   EXPAND(o); FILL(o);
   elm_object_style_set(o, style);
   elm_object_text_set(o, "To: ");
   elm_object_part_text_set(o, "guide", "Tap to add recipient");
   evas_object_show(o);

   return o;
}

#define STD_ICON_NUM 8
static int navi_item_count = 0;
static const char *std_icons[20] =
{
   "edit", "delete", "clock", "arrow_right", "arrow_left",
   "refresh", "folder", "file"
};
static void
_naviframe_prev_btn_cb(void *data, Evas_Object *obj EINA_UNUSED,
                       void *event_info EINA_UNUSED)
{
   Evas_Object *o = data;

   navi_item_count--;
   elm_naviframe_item_pop(o);
}

static void
_naviframe_next_btn_cb(void *data, Evas_Object *obj EINA_UNUSED,
                       void *event_info EINA_UNUSED)
{
   Evas_Object *o = data, *icon = NULL, *next_btn = NULL, *prev_btn = NULL;
   const char *style = NULL;
   char buf[PATH_MAX] = { 0 };

   if (!o) return;

   style = evas_object_data_get(o, "style");
   if (!style) return;

   navi_item_count++;
   snprintf(buf, PATH_MAX, "Navi Item #%d", navi_item_count);

   icon = elm_icon_add(o);
   elm_icon_standard_set(icon, std_icons[navi_item_count % STD_ICON_NUM]);
   evas_object_show(icon);

   if (navi_item_count != 1)
     {
        prev_btn = elm_button_add(o);
        elm_object_text_set(prev_btn, "Prev");
        evas_object_smart_callback_add(prev_btn, "clicked", _naviframe_prev_btn_cb, o);
        evas_object_show(prev_btn);
     }

   next_btn = elm_button_add(o);
   elm_object_text_set(next_btn, "Next");
   evas_object_smart_callback_add(next_btn, "clicked", _naviframe_next_btn_cb, o);
   evas_object_show(next_btn);

   elm_naviframe_item_push(o, buf, prev_btn, next_btn, icon, style);
}

// naviframe
static Evas_Object *
_widget_naviframe_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o = NULL;

   o = elm_naviframe_add(parent);
   EXPAND(o); FILL(o);
   evas_object_data_set(o, "style", style);
   evas_object_show(o);

   navi_item_count = 0;
   _naviframe_next_btn_cb(o, NULL, NULL);

   return o;
}

static void
_widget_notify_response_cb(void *data EINA_UNUSED, Evas_Object *obj,
                           void *event_info EINA_UNUSED)
{
   evas_object_del(obj);
}

static void
_widget_notify_close_cb(void *data, Evas_Object *obj EINA_UNUSED,
                        void *event_info EINA_UNUSED)
{
   evas_object_del(data);
}

static void
_widget_notify_launch(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED,
                      void *event_info EINA_UNUSED)
{
   Evas_Object *o = NULL, *box = NULL, *lbl = NULL, *btn = NULL;

   o = elm_notify_add(gd->win);
   evas_object_smart_callback_add(o, "block,clicked",
                                  _widget_notify_response_cb, NULL);

   if (wod)
     {
        elm_notify_allow_events_set(o, wod->notify_event);
        elm_notify_align_set(o, wod->notify_horz_align, wod->notify_vert_align);
        INF("notify align : %f %f\n", wod->notify_horz_align, wod->notify_vert_align);
        //elm_notify_timeout_set
     }
   evas_object_show(o);

   box = elm_box_add(o);
   elm_object_content_set(o, box);
   evas_object_show(box);

   lbl = elm_label_add(box);
   elm_object_text_set(lbl, "This is a notify widget.");
   elm_box_pack_end(box, lbl);
   evas_object_show(lbl);

   btn = elm_button_add(box);
   elm_object_text_set(btn, "Close");
   evas_object_smart_callback_add(btn, "clicked", _widget_notify_close_cb, o);
   elm_box_pack_end(box, btn);
   evas_object_show(btn);
}

// notify
static Evas_Object *
_widget_notify_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o = NULL;

   o = elm_button_add(parent);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_object_text_set(o, "Press to launch a notify");
   evas_object_smart_callback_add(o, "clicked", _widget_notify_launch, style);
   evas_object_show(o);

   return o;
}

// panel
static Evas_Object *
_widget_panel_create(Evas_Object *parent, const char *orig_style)
{
   Evas_Object *o, *lbl;
   char style[PATH_MAX] = {0, };

   lbl = elm_label_add(parent);
   elm_label_line_wrap_set(lbl, ELM_WRAP_MIXED);
   elm_object_text_set(lbl, "This is a panel.<br>"
                       "Panel has four orientation:<br>"
                       "TOP, BOTTOM, LEFT, RIGHT");
   evas_object_show(lbl);

   o = elm_panel_add(parent);
   elm_object_content_set(o, lbl);
   evas_object_show(o);

   _trim_end_default(orig_style, style);
   if (!strcmp(style, "bottom"))
     {
        EXPAND(o); ALIGN(o, EVAS_HINT_FILL, 1.0);
        elm_panel_orient_set(o, ELM_PANEL_ORIENT_BOTTOM);
     }
   else if (!strcmp(style, "top"))
     {
        EXPAND(o); ALIGN(o, EVAS_HINT_FILL, 0);
        elm_panel_orient_set(o, ELM_PANEL_ORIENT_TOP);
     }
   else if (!strcmp(style, "left"))
     {
        EXPAND(o); ALIGN(o, 0.0, EVAS_HINT_FILL);
        elm_panel_orient_set(o, ELM_PANEL_ORIENT_LEFT);
     }
   else if (!strcmp(style, "right"))
     {
        EXPAND(o); ALIGN(o, 1.0, EVAS_HINT_FILL);
        elm_panel_orient_set(o, ELM_PANEL_ORIENT_RIGHT);
     }

   return o;
}

// photo
static Evas_Object *
_widget_photo_create(Evas_Object *parent, const char *style)
{
   char buf[PATH_MAX];
   Evas_Object *o;

   o = elm_photo_add(parent);
   EXPAND(o); ALIGN(o, 0.5, 0.5);
   elm_photo_aspect_fixed_set(o, EINA_FALSE);
   elm_object_style_set(o, style);
   elm_photo_size_set(o, WIDGET_DEFAULT_WIDTH);
   snprintf(buf, sizeof(buf), "%s/images/sky_01.jpg", elm_app_data_dir_get());
   elm_photo_file_set(o, buf);
   evas_object_show(o);

   return o;
}

// player
static Evas_Object *
_widget_player_create(Evas_Object *parent, const char *style EINA_UNUSED)
{
   Evas_Object *o = NULL;

   o = elm_player_add(parent);
   evas_object_show(o);

   return o;
}

// pointer
static Evas_Object *
_widget_pointer_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o;

   o = elm_layout_add(parent);
   elm_layout_theme_set(o, "pointer", "base", style);
   evas_object_show(o);

   return o;
}

// popup
static void
_widget_popup_response_cb(void *data EINA_UNUSED, Evas_Object *obj,
                          void *event_info EINA_UNUSED)
{
   evas_object_del(obj);
}

static void
_widget_popup_close_cb(void *data, Evas_Object *obj EINA_UNUSED,
                       void *event_info EINA_UNUSED)
{
   evas_object_del((Evas_Object *)data);
}

#define WIDGET_POPUP_BUTTON_CREATE(text__, btn_num__) \
   do { \
     btn = elm_button_add(o); \
     elm_object_text_set(btn, text__); \
     elm_object_part_content_set(o, btn_num__, btn); \
     evas_object_show(btn); \
     evas_object_smart_callback_add(btn, "clicked", _widget_popup_close_cb, o); \
   } while (0)

static void
_widget_popup_launch(void *data, Evas_Object *obj EINA_UNUSED,
                     void *event_info EINA_UNUSED)
{
   Evas_Object *o = NULL, *btn = NULL, *icon = NULL, *rect = NULL;
   char buf[PATH_MAX] = { 0 };

   o = elm_popup_add(gd->win);
   elm_object_scale_set(o, option_scale_get());
   elm_object_theme_set(o, th);
   elm_object_style_set(o, data);
   evas_object_smart_callback_add(o, "block,clicked",
                                  _widget_popup_response_cb, NULL);
   if (wod)
     {
        switch (wod->pop_button)
          {
           case 1:
             WIDGET_POPUP_BUTTON_CREATE("OK", "button1");
             break;
           case 2:
             WIDGET_POPUP_BUTTON_CREATE("OK", "button1");
             WIDGET_POPUP_BUTTON_CREATE("Cancel", "button2");
             break;
           case 3:
             WIDGET_POPUP_BUTTON_CREATE("OK", "button1");
             WIDGET_POPUP_BUTTON_CREATE("Cancel", "button2");
             WIDGET_POPUP_BUTTON_CREATE("Close", "button3");
             break;
           default:
             break;
          }
        if (wod->pop_title_text)
          elm_object_part_text_set(o, "title,text", "Title Text");
        switch (wod->pop_content_type)
          {
           case 0:
             elm_object_text_set(o, "Popup Text Short");
             break;
           case 1:
             elm_object_text_set(o, "This is a popup object and this area is "
                                 "content area. Popup can handle multiline "
                                 "texts.");
             break;
           case 2:
             btn = elm_button_add(o);
             elm_object_text_set(btn, "Popup Content as a Button");
             evas_object_show(btn);
             elm_object_content_set(o, btn);
             break;
           case 3:
             rect = evas_object_rectangle_add(evas_object_evas_get(o));
             evas_object_color_set(rect, 100, 0, 0, 100);
             evas_object_size_hint_min_set(rect, 200, 50);
             evas_object_show(rect);
             elm_object_content_set(o, rect);
             break;
           case 4:
             rect = evas_object_rectangle_add(evas_object_evas_get(o));
             evas_object_color_set(rect, 100, 0, 0, 100);
             evas_object_size_hint_min_set(rect, 500, 200);
             evas_object_show(rect);
             elm_object_content_set(o, rect);
             break;
           default:
             break;
          }
        if (wod->pop_title_icon)
          {
              icon = elm_icon_add(o);
              snprintf(buf, sizeof(buf), "%s/images/logo_small.png",
                       elm_app_data_dir_get());
              elm_image_file_set(icon, buf, NULL);
              evas_object_size_hint_aspect_set(icon,
                                               EVAS_ASPECT_CONTROL_VERTICAL,
                                               1, 1);
              elm_object_part_content_set(o, "title,icon", icon);
          }

        // orient set
        elm_popup_orient_set(o, wod->pop_orient);
     }
   else
     {
        elm_object_part_text_set(o, "title,text", "Title Text");
        elm_object_text_set(o, "This is a popup object.");
     }

   evas_object_show(o);
}

#undef WIDGET_POPUP_BUTTON_CREATE

static Evas_Object *
_widget_popup_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o = NULL;
   o = elm_button_add(parent);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_object_text_set(o, "Press to lauch a popup");
   evas_object_smart_callback_add(o, "clicked", _widget_popup_launch, style);
   evas_object_show(o);

   return o;
}

static void
_progressbar_del_cb(void *data EINA_UNUSED, Evas *e EINA_UNUSED,
                    Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   ETV_SAFE_FREE(widget_timer, ecore_timer_del);
}

static Eina_Bool
_progressbar_value_set(void *data)
{
   Evas_Object *obj = data;
   double progress = elm_progressbar_value_get(obj);

   if (progress < 1.0) progress += 0.055;
   else progress = 0.0;
   elm_progressbar_value_set(obj, progress);

   return ECORE_CALLBACK_RENEW;
}

// progressbar
static Evas_Object *
_widget_progressbar_create(Evas_Object *parent, const char *style2)
{
   char buf[PATH_MAX] = {0, };
   const char *style;
   Evas_Object *o;

   o = elm_progressbar_add(parent);
   EXPAND(o); FILL(o);
   evas_object_show(o);

   strncpy(buf, style2, sizeof(buf) - 1);
   style = strtok(buf, "/");
   if (!strcmp("vertical", style))
     elm_progressbar_horizontal_set(o, EINA_FALSE);
   style = strtok(NULL, "/");

   elm_object_style_set(o, style);
   elm_object_text_set(o, style);

   if (wod)
     {
        if (wod->pg_pulse)
          {
             elm_progressbar_pulse_set(o, EINA_TRUE);
             elm_progressbar_pulse(o, EINA_TRUE);
          }
        else
          {
             widget_timer = ecore_timer_add(0.1, _progressbar_value_set, o);
          }
     }
   else
     {
        elm_progressbar_pulse_set(o, EINA_TRUE);
        elm_progressbar_pulse(o, EINA_TRUE);
     }
   evas_object_event_callback_add(o, EVAS_CALLBACK_DEL,
                                  _progressbar_del_cb, NULL);

   return o;
}

// radio
static void
_widget_radio_radio_object_create(Evas_Object *box, int value, const char *str, const char *style)
{
   Evas_Object *o = NULL;
   static Evas_Object *rdg = NULL;

   o = elm_radio_add(box);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_radio_state_value_set(o, value);
   elm_object_style_set(o, style);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   if (wod)
     {
        if (wod->rd_text)
          elm_object_text_set(o, str);
        if (wod->rd_image)
          {
             char buf[PATH_MAX] = { 0 };
             Evas_Object *icon = elm_icon_add(o);
             snprintf(buf, sizeof(buf), "%s/images/logo_small.png",
                      elm_app_data_dir_get());
             elm_image_file_set(icon, buf, NULL);
             evas_object_size_hint_aspect_set(icon, EVAS_ASPECT_CONTROL_VERTICAL,
                                              1, 1);
             evas_object_show(icon);
             elm_object_content_set(o, icon);
          }
     }
   else
     {
        elm_object_text_set(o, str);
     }

   if (value == 1) rdg = o;
   else elm_radio_group_add(o, rdg);

   elm_radio_value_set(rdg, 1);
}

static Evas_Object *
_widget_radio_create(Evas_Object *parent, const char *style)
{
   Evas_Object *box, *o;

   box = o = elm_box_add(parent);
   EXPAND(o); FILL(o);
   evas_object_show(o);

   _widget_radio_radio_object_create(box, 1, "Tiffany", style);
   _widget_radio_radio_object_create(box, 2, "Jessica", style);
   _widget_radio_radio_object_create(box, 3, "Yuna", style);

   return box;
}

// separator
static Evas_Object *
_widget_separator_create(Evas_Object *parent, const char *orig_style)
{
   Evas_Object *o;

   char buf[PATH_MAX] = {0, };
   const char *type;

   strncpy(buf, orig_style, sizeof(buf) - 1);
   type = strtok(buf, "/");
   //INF("%s", type);

   o = elm_separator_add(parent);
   if (!strcmp("horizontal", type))
     {
        ALIGN(o, EVAS_HINT_FILL, 0);
        elm_separator_horizontal_set(o, EINA_TRUE);
     }
   else
     ALIGN(o, 0, EVAS_HINT_FILL);
   evas_object_show(o);

   return o;
}

// slider
static Evas_Object *
_widget_slider_create(Evas_Object *parent, const char *orig_style, const char *style)
{
   Evas_Object *o;

   o = elm_slider_add(parent);
   ALIGN(o, 0.5, 0.5);
   elm_object_style_set(o, style);
   elm_slider_min_max_set(o, 50, 200);
   elm_slider_value_set(o, 80);
   elm_slider_unit_format_set(o, "%3.0f units");
   elm_slider_span_size_set(o, 120);
   evas_object_show(o);

   if ((strlen(orig_style) > 8) && !strncmp("vertical", orig_style, 8))
     elm_slider_horizontal_set(o, EINA_FALSE);

   if (wod)
     {
        if (wod->sl_text)
          elm_object_text_set(o, "This is a Slider");

        if (wod->sl_indicator)
          elm_slider_indicator_format_set(o, "%3.0f");
        else
          elm_slider_indicator_show_set(o, EINA_FALSE);

        if (wod->sl_image)
          {
             char buf[PATH_MAX] = { 0 };
             Evas_Object *icon = elm_icon_add(o);
             snprintf(buf, sizeof(buf), "%s/images/logo_small.png",
                      elm_app_data_dir_get());
             elm_image_file_set(icon, buf, NULL);
             evas_object_size_hint_aspect_set(icon,
                                              EVAS_ASPECT_CONTROL_VERTICAL,
                                              1, 1);
             evas_object_show(icon);
             elm_object_content_set(o, icon);
          }
     }
   else
     {
        elm_object_text_set(o, "This is a Slider");
        elm_slider_indicator_format_set(o, "%3.0f");
     }

   return o;
}

// spinner
static Evas_Object *
_widget_spinner_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o;

   o = elm_spinner_add(parent);

   elm_spinner_min_max_set(o, -50.0, 250.0);
   //EXPAND(o);
   ALIGN(o, EVAS_HINT_FILL, 0.5);

   elm_spinner_label_format_set(o, "%1.1f units");
   elm_spinner_step_set(o, 1.3);
   elm_spinner_wrap_set(o, EINA_TRUE);

   if (!strcmp("vertical", style))
     elm_object_style_set(o, "vertical");
   evas_object_show(o);

   return o;
}

// thumb
static Evas_Object *
_widget_thumb_create(Evas_Object *parent, const char *style)
{
   char buf[PATH_MAX];
   Evas_Object *o;

   elm_need_ethumb();

   o = elm_thumb_add(parent);
   snprintf(buf, sizeof(buf), "%s/images/sky_01.jpg", elm_app_data_dir_get());
   elm_thumb_file_set(o, buf, NULL);
   elm_object_style_set(o, style);
   evas_object_size_hint_min_set(o, 200, 200);
   evas_object_show(o);

   return o;
}

// toolbar
static Evas_Object *
_widget_toolbar_create(Evas_Object *parent, const char *orig_style, const char *style)
{
   Evas_Object *o;
   char style2[PATH_MAX] = {0, };

   _trim_end_default(orig_style, style2);

   o = elm_toolbar_add(parent);
   EXPAND(o); ALIGN(o, EVAS_HINT_FILL, 0.0);

   if (!strcmp("item_centered", style))
     elm_object_style_set(o, "item_centered");
   else if (!strcmp("item_horizontal", style))
     elm_object_style_set(o, "item_horizontal");

   elm_toolbar_item_append(o, "document-print", "Item 1", NULL, NULL);
   elm_toolbar_item_append(o, "folder-new", "Item 2", NULL, NULL);

   if (!strcmp("separator", style2))
     {
        elm_toolbar_item_separator_set(elm_toolbar_item_append(o, NULL, NULL, NULL, NULL),
                                       EINA_TRUE);
     }
   elm_toolbar_item_append(o, "object-rotate-right", "Item 3", NULL, NULL);
   elm_toolbar_item_append(o, "clock", "Item 4", NULL, NULL);

   if (!strcmp("separator", style2))
     {
        elm_toolbar_item_separator_set(elm_toolbar_item_append(o, NULL, NULL, NULL, NULL),
                                       EINA_TRUE);
     }

   elm_toolbar_item_append(o, "clock", "Item 5", NULL, NULL);
   elm_toolbar_item_append(o, "clock", "Item 6", NULL, NULL);
   elm_toolbar_item_append(o, "clock", "Item 7", NULL, NULL);
   evas_object_show(o);

   if (wod)
     {
        elm_toolbar_shrink_mode_set(o, wod->tb_shrink);
     }

   return o;
}

static Evas_Object *
_tooltip_content_cb(void *data EINA_UNUSED,
                    Evas_Object *obj EINA_UNUSED,
                    Evas_Object *tt)
{
   Evas_Object *o;
   char buf[PATH_MAX];

   o = elm_icon_add(tt);
   snprintf(buf, sizeof(buf), "%s/images/sky_01.jpg", elm_app_data_dir_get());
   elm_image_file_set(o, buf, NULL);
   evas_object_size_hint_min_set(o, WIDGET_DEFAULT_WIDTH, WIDGET_DEFAULT_HEIGHT);
   evas_object_show(o);

   return o;
}

// tooltip
static Evas_Object *
_widget_tooltip_create(Evas_Object *parent, const char *style)
{
   Evas_Object *o, *box;

   box = o = elm_box_add(parent);
   EXPAND(o); FILL(o);
   evas_object_show(o);

   o = elm_bg_add(box);
   EXPAND(o); FILL(o);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   o = elm_button_add(parent);
   FILL(o);
   elm_object_text_set(o, "(Text) Mouse over here to see the tooltip");
   elm_object_tooltip_text_set(o, "Simple Text Tooltip");
   elm_object_tooltip_style_set(o, style);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   o = elm_button_add(parent);
   FILL(o);
   elm_object_text_set(o, "(Icon) Mouse over here to see the tooltip");
   elm_object_tooltip_content_cb_set(o, _tooltip_content_cb, NULL, NULL);
   elm_object_tooltip_style_set(o, style);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   o = elm_bg_add(box);
   EXPAND(o); FILL(o);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   return box;
}

// win
static void
_inwin_close_btn_clicked_cb(void *data, Evas_Object *obj EINA_UNUSED,
                            void *event_info EINA_UNUSED)
{
   if (!data) return;
   evas_object_del(data);
}

static void
_widget_win_launch(void *data, Evas_Object *obj EINA_UNUSED,
                   void *event_info EINA_UNUSED)
{
   Evas_Object *o = NULL, *inwin = NULL, *box = NULL;

   inwin = o = elm_win_inwin_add(gd->win);
   elm_object_style_set(o, data);
   evas_object_show(o);

   box = o = elm_box_add(inwin);
   evas_object_size_hint_weight_set(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   elm_win_inwin_content_set(inwin, o);
   evas_object_show(o);

   o = elm_label_add(box);
   elm_object_text_set(o,
                       "This is an \"inwin\" - a window in a<br/>"
                       "window. This is handy for quick popups<br/>"
                       "you want centered, taking over the window<br/>"
                       "until dismissed somehow. Unlike hovers they<br/>"
                       "don't hover over their target.");
   elm_box_pack_end(box, o);
   evas_object_show(o);

   o = elm_button_add(box);
   elm_object_text_set(o, "Close");
   elm_box_pack_end(box, o);
   evas_object_smart_callback_add(o, "clicked",
                                  _inwin_close_btn_clicked_cb, inwin);
   evas_object_show(o);
}

static Evas_Object *
_widget_win_create(Evas_Object *parent EINA_UNUSED, const char *style)
{
   Evas_Object *o = NULL;
   o = elm_button_add(parent);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_object_text_set(o, "Press to lauch a Window");
   evas_object_smart_callback_add(o, "clicked", _widget_win_launch, style);
   evas_object_show(o);
   return o;
}

void
widget_resize(Evas_Object *o)
{
   Evas_Coord w = 0, h = 0;
   Evas_Object *rect = NULL;

   if (option_force_resize_get())
     {
        option_size_get(&w, &h);
        rect = evas_object_data_get(o, "table_rect");
        // support the special case that the min is set
        // by table and rect combination
        if (rect)
          {
             evas_object_size_hint_min_set(rect, w, h);
             evas_object_size_hint_max_set(rect, w, h);
          }
        // in normal cases just set the min directly to the object
        else
          {
             evas_object_size_hint_min_set(o, w, h);
             evas_object_size_hint_max_set(o, w, h);
          }
     }
}

Evas_Object *
widget_create(Evas_Object *parent, Widget_Type widget, const char *orig_style)
{
   Evas_Object *o = NULL;
   const char *style = NULL;

   if (!orig_style) return NULL;

   style = eina_stringshare_add(_style_split_2(orig_style));
   if (!style)
     style = eina_stringshare_add("default");
   //INF("widget : %s, orig_style : %s, style : %s", widget, orig_style, style);

#define ADD_WIDGET(name__, id__, style__) \
   else if (widget == id__) \
    o = _widget_##name__##_create(parent, style__)
#define ADD_WIDGET2(name__, id__, style1__, style2__) \
   else if (widget == id__) \
    o = _widget_##name__##_create(parent, style1__, style2__)

   if (widget == ETV_ID_ACTIONSLIDER)
     o = _widget_actionslider_create(parent, style);
   ADD_WIDGET(access, ETV_ID_ACCESS, style);
   ADD_WIDGET(bg, ETV_ID_BG, style);
   ADD_WIDGET(border, ETV_ID_BORDER, style);
   ADD_WIDGET(bubble, ETV_ID_BUBBLE, orig_style);
   ADD_WIDGET(button, ETV_ID_BUTTON, orig_style);
   ADD_WIDGET(calendar, ETV_ID_CALENDAR, style);
   ADD_WIDGET(check, ETV_ID_CHECK, style);
   ADD_WIDGET(clock, ETV_ID_CLOCK, style);
   ADD_WIDGET(colorselector, ETV_ID_COLORSELECTOR, style);
   ADD_WIDGET(ctxpopup, ETV_ID_CTXPOPUP, style);
   ADD_WIDGET(datetime, ETV_ID_DATETIME, style);
   ADD_WIDGET2(entry, ETV_ID_ENTRY, orig_style, style);
   ADD_WIDGET(ews, ETV_ID_EWS, orig_style);
   ADD_WIDGET(fileselector, ETV_ID_FILESELECTOR, style);
   ADD_WIDGET(fileselector_entry, ETV_ID_FILESELECTOR_ENTRY, style);
   ADD_WIDGET(flipselector, ETV_ID_FLIPSELECTOR, style);
   ADD_WIDGET(focus_highlight, ETV_ID_FOCUS_HIGHLIGHT, orig_style);
   ADD_WIDGET(frame, ETV_ID_FRAME, style);
   ADD_WIDGET2(gengrid, ETV_ID_GENGRID, orig_style, style);
   ADD_WIDGET2(genlist, ETV_ID_GENLIST, orig_style, style);
   ADD_WIDGET(hover, ETV_ID_HOVER, style);
   ADD_WIDGET(index, ETV_ID_INDEX, style);
   ADD_WIDGET(icon, ETV_ID_ICON, orig_style);
   ADD_WIDGET(label, ETV_ID_LABEL, style);
   ADD_WIDGET(layout, ETV_ID_LAYOUT, style);
   ADD_WIDGET2(list, ETV_ID_LIST, orig_style, style);
   ADD_WIDGET(menu, ETV_ID_MENU, orig_style);
   ADD_WIDGET(multibuttonentry, ETV_ID_MULTIBUTTONENTRY, style);
   ADD_WIDGET(naviframe, ETV_ID_NAVIFRAME, style);
   ADD_WIDGET(notify, ETV_ID_NOTIFY, style);
   ADD_WIDGET(panel, ETV_ID_PANEL, orig_style);
   ADD_WIDGET(photo, ETV_ID_PHOTO, style);
   ADD_WIDGET(player, ETV_ID_PLAYER, style);
   ADD_WIDGET(pointer, ETV_ID_POINTER, style);
   ADD_WIDGET(popup, ETV_ID_POPUP, style);
   ADD_WIDGET(progressbar, ETV_ID_PROGRESSBAR, orig_style);
   ADD_WIDGET(radio, ETV_ID_RADIO, style);
   ADD_WIDGET(separator, ETV_ID_SEPARATOR, orig_style);
   ADD_WIDGET2(slider, ETV_ID_SLIDER, orig_style, style);
   ADD_WIDGET(spinner, ETV_ID_SPINNER, style);
   ADD_WIDGET(thumb, ETV_ID_THUMB, style);
   ADD_WIDGET2(toolbar, ETV_ID_TOOLBAR, orig_style, style);
   ADD_WIDGET(tooltip, ETV_ID_TOOLTIP, style);
   ADD_WIDGET(win, ETV_ID_WIN, style);
   else
     o = _widget_not_implemented_create(parent, widget);

   // o can be NULL (ex: inwin)
   if (!o) return NULL;

   elm_object_theme_set(o, th);

   elm_object_scale_set(o, option_scale_get());
   widget_resize(o);
   if (option_disabled_get())
     elm_object_disabled_set(o, EINA_TRUE);

   return o;
}

Widget_Type
widget_type_get_by_name(const char *widget)
{
   int i = 0;
   if (!widget) return ETV_ID_NONE;

   while (widgets[i].type != ETV_ID_LAST)
     {
        if (!strcmp(widgets[i].name, widget))
          return widgets[i].type;
        i++;
     }
   return ETV_ID_NONE;
}

const char *
widget_name_get_by_type(Widget_Type type)
{
   if (type < WIDGET_COUNT)
     return widgets[type].name;
   else
     return NULL;
}

const char *
widget_desc_get_by_type(Widget_Type type)
{
   if (type < WIDGET_COUNT)
     return widgets[type].desc;
   else
     return NULL;
}
