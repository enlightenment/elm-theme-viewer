#ifndef __INCLUDE_UTIL__
#define __INCLUDE_UTIL__

/*
 * Set elementary widget's min size.
 * We should not set min size hint to elementary widgets because elementary has
 * its own size policy/logic. This is an official trick from raster.
 * @param obj the actual object that you want to set the min size
 * @param w min width
 * @param h min height
 */
Evas_Object *util_elm_min_set(Evas_Object *obj, Evas_Coord w, Evas_Coord h);

#endif
