#include <Elementary.h>
#include "common.h"

extern Gui_Data *gd;

Evas_Object *
util_elm_min_set(Evas_Object *obj, Evas_Coord w, Evas_Coord h)
{
   Evas_Object *table, *rect;

   table = elm_table_add(gd->win);
   evas_object_show(table);

   rect = evas_object_rectangle_add(evas_object_evas_get(table));
   evas_object_size_hint_min_set(rect,
                                 w * elm_config_scale_get(),
                                 h * elm_config_scale_get());
   EXPAND(rect); FILL(rect);
   elm_table_pack(table, rect, 0, 0, 1, 1);

   EXPAND(obj); FILL(obj);
   elm_table_pack(table, obj, 0, 0, 1, 1);

   evas_object_data_set(table, "table_rect", rect);

   return table;
}
