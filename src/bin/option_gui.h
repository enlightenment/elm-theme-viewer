#ifndef __INCLUDE_GUI_OPTION__
#define __INCLUDE_GUI_OPTION__

typedef struct _Option_Gui_Data Option_Gui_Data;
struct _Option_Gui_Data
{
   Evas_Object *size_width_slider;
   Evas_Object *size_height_slider;
   Eina_Bool option_force_resize_prev;
   Evas_Coord option_size_width_prev;
   Evas_Coord option_size_height_prev;
   double option_scale_prev;
};

void option_gui_finger_size_create(Evas_Object *box);
void option_gui_scale_create(Evas_Object *box);
void option_gui_force_resize_create(Evas_Object *box);
void option_gui_width_size_create(Evas_Object *box);
void option_gui_height_size_create(Evas_Object *box);
void option_gui_create(void);

#endif
