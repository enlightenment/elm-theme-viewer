#include <Elementary.h>
#include "common.h"

Eina_List *widget_list;
Elm_Theme *th;

void
theme_init(void)
{
   int i = 0;
   Widget_Data *wd = NULL;

   if (widget_list) return;

   // TODO : create only necessary widget data. do not create all wd for unused widgets.
   while (widgets[i].type < ETV_ID_LAST)
     {
        wd = (Widget_Data *)calloc(1, sizeof(Widget_Data));
        wd->type = widgets[i].type;
        widget_list = eina_list_append(widget_list, wd);
        i++;
     }
   INF("Theme Init Done");
}

void
theme_shutdown(void)
{
   Widget_Data *wd = NULL;
   Widget_Data_Style *wds = NULL;

   EINA_LIST_FREE(widget_list, wd)
     {
        EINA_LIST_FREE(wd->styles, wds)
          {
             eina_stringshare_del(wds->group);
             eina_stringshare_del(wds->style);
             eina_stringshare_del(wds->simple);
          }
        free(wd);
     }
}

void
theme_set(const char *edje_file)
{
   if (!edje_file) return;

   th = elm_theme_new();
   elm_theme_ref_set(th, NULL);
   elm_theme_overlay_add(th, edje_file);
}

void
theme_unset(const char *edje_file)
{
   if (!edje_file) return;

   elm_theme_overlay_del(th, edje_file);
   elm_theme_free(th);
}

static int
_style_compare_cb(const void *data1, const void *data2)
{
   const char *txt1 = data1;
   const char *txt2 = data2;

   if (!txt1) return 1;
   if (!txt2) return -1;

   return strcmp(txt1, txt2);
}

void
theme_load(const char *edje_file, Eina_Bool list)
{
   Eina_List *groups = NULL, *l = NULL, *ll = NULL;
   char *group = NULL, *token = NULL, *widget = NULL;
   char *style = NULL;
   char buf[PATH_MAX] = {0, };
   Widget_Data *wd = NULL;
   Widget_Data_Style *wds = NULL;
   Eina_Compare_Cb cmp_func = (Eina_Compare_Cb)_style_compare_cb;

   if (!edje_file) return;

   groups = edje_file_collection_list(edje_file);
   if (!groups) return;

   groups = eina_list_sort(groups, eina_list_count(groups), cmp_func);

   EINA_LIST_FOREACH(groups, l, group)
     {
        strncpy(buf, group, sizeof(buf) - 1);

        token = strtok(buf, "/");
        if (strncmp("elm", token, 5))
          {
             //ERR("%s is not a proper elementary style.", group);
             continue;
          }

        // get the widget name
        widget = strtok(NULL, "/");
        if (!widget) continue;

        // get the widget data of the widget
        wd = NULL;
        EINA_LIST_FOREACH(widget_list, ll, wd)
          {
             if (!strcmp(widget_name_get_by_type(wd->type), widget))
               break;
          }
        if (!wd)
          {
             //ERR("%s is not a proper elementary widget.", widget);
             continue;
          }

        // get the style name
        style = strstr(group, "/");
        style = strstr(style + 1, "/");
        style++;

        //INF("%s %s %p", group, style, wd);
        wds = (Widget_Data_Style *)calloc(1, sizeof(Widget_Data_Style));
        wds->group = eina_stringshare_add(group);
        wds->style = eina_stringshare_add(style);
        wds->simple = eina_stringshare_add(
           widget_style_filter(wd->type, style));

        if (!strcmp("default", wds->simple))
          wd->styles = eina_list_prepend(wd->styles, wds);
        else
          wd->styles = eina_list_append(wd->styles, wds);

        if (list)
          printf("Widget: %-20s\t\tStyle: %s\n", widget, style);
     }

   edje_file_collection_list_free(groups);

   INF("Theme Load Done");
}

void
theme_widgets_print(void)
{
   Eina_List *l;
   Widget_Data *wd;

   EINA_LIST_FOREACH(widget_list, l, wd)
     INF("%s %d", widget_name_get_by_type(wd->type), eina_list_count(wd->styles));
}

Eina_List *
theme_widget_styles_get(Widget_Type widget)
{
   Widget_Data *wd = NULL;
   Eina_List *l;

   if (!widget) return NULL;

   EINA_LIST_FOREACH(widget_list, l, wd)
     {
        if (widget == wd->type)
          break;
     }
   return wd->styles;
}

void
theme_widget_styles_print(Widget_Type widget)
{
   Eina_List *l = NULL;
   Widget_Data *wd = NULL;
   char *style = NULL;

   EINA_LIST_FOREACH(widget_list, l, wd)
     {
        if (widget == wd->type)
          break;
     }

   EINA_LIST_FOREACH(wd->styles, l, style)
     INF("%s", style);
}
