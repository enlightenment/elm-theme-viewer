#ifndef __INCLUDE_WIDGET_OPTION__
#define __INCLUDE_WIDGET_OPTION__

void widget_option_content_update(Widget_Type type);

typedef struct _Widget_Option_Data Widget_Option_Data;
struct _Widget_Option_Data
{
   // actionslider
   Eina_Bool actslider_magnet_pos_left;
   Eina_Bool actslider_magnet_pos_center;
   Eina_Bool actslider_magnet_pos_right;
   int actslider_indi_pos; // indicator position

   // button
   Eina_Bool btn_image : 1;
   Eina_Bool btn_text : 1;

   // check
   Eina_Bool chk_selected : 1;
   Eina_Bool chk_image : 1;
   Eina_Bool chk_text : 1;

   // datetime
   Eina_Bool dt_year : 1; // ELM_DATETIME_YEAR
   Eina_Bool dt_month : 1; // ELM_DATETIME_MONTH
   Eina_Bool dt_date : 1; // ELM_DATETIME_DATE
   Eina_Bool dt_hour : 1; // ELM_DATETIME_HOUR
   Eina_Bool dt_minute : 1; // ELM_DATETIME_MINUTE
   Eina_Bool dt_ampm : 1; // ELM_DATETIME_AMPM

   // entry
   int entry_text_len;
   Eina_Bool entry_guide_text : 1;

   // gengrid
   int gengrid_item_width;
   int gengrid_item_height;
   int gengrid_text_len; // short (0), middle (1), long (2)
   double gengrid_vert_align;
   double gengrid_horz_align;
   int gengrid_select_mode; // default (0), always (1), none (2), display_only (3)

   // genlist
   int genlist_text_len; // short (0), middle (1), long (2)
   int genlist_content_type; // icon (0), check (1), button (2)
   int genlist_select_mode; // default (0), always (1), none (2), display_only (3)

   // notify
   Eina_Bool notify_event : 1; // disallow events(0), allow events (1)
   double notify_horz_align;
   double notify_vert_align;

   // popup
   int pop_content_type; // content: short text (0), long text (1), button (2) and etc.
   int pop_button; // number of popup buttons (1 ~ 3)
   Eina_Bool pop_title_text : 1; // disable (0) or enable (1) title text
   Eina_Bool pop_title_icon : 1; // disable (0) or enable (1) title icon
   int pop_orient; // popup orient (Elm_Popup_Orient)

   // progressbar
   Eina_Bool pg_pulse : 1; // pulse (1) or progress (0)

   // radio
   Eina_Bool rd_image : 1;
   Eina_Bool rd_text : 1;

   // slider
   Eina_Bool sl_text : 1;
   Eina_Bool sl_image : 1;
   Eina_Bool sl_indicator : 1;

   // toolbar
   Elm_Toolbar_Shrink_Mode tb_shrink;
};
#endif
