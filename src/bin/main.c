#include <Elementary.h>
#include <Ecore_Getopt.h>
#include "common.h"

ETV_Data *ed = NULL;
static const Ecore_Getopt options = {
   PACKAGE_NAME,
   "%prog [options]",
   PACKAGE_VERSION,
   "(C) 2012-2015 Daniel Juyung Seo and others",
   "BSD 2-Clause",
   "Elementary theme previewer written with Enlightenment Foundation Libraries.",
   EINA_TRUE,
   {
      ECORE_GETOPT_STORE_STR('t', "theme",
                             "Set the theme to load and parse."),
      //ECORE_GETOPT_STORE_TRUE('m', "mobile", "Set the mobile view."), // TODO: enable in v0.2
      ECORE_GETOPT_STORE_STR('s', "screensize", "Set the screen size in "
                             "WIDTHxHEIGHT format. (300x500)"),
      ECORE_GETOPT_STORE_TRUE('F', "fullscreen",
                              "Go into the fullscreen mode from start."),
      /*ECORE_GETOPT_STORE_TRUE('T', "tizen",
                              "Run as a Tizen platform mode. Use Tizen "
                              "internal styles. This enables mobile mode "
                              "automatically."),*/ // TODO: enable in v0.2
      ECORE_GETOPT_STORE_STR('w', "widget",
                             "Set the widget to launch from command line."),
      ECORE_GETOPT_STORE_TRUE('l', "list",
                              "Lists up all the styles in the theme file and exit."),
      ECORE_GETOPT_VERSION  ('V', "version"),
      ECORE_GETOPT_COPYRIGHT('C', "copyright"),
      ECORE_GETOPT_LICENSE  ('L', "license"),
      ECORE_GETOPT_HELP     ('h', "help"),
      ECORE_GETOPT_SENTINEL
   }
};

int
main(int argc, char **argv)
{
   const char *edje_file = NULL;
   int gui_init_ret = 0;
   int option_init_ret = 0;

   int args;
   char *theme = NULL;
   char *screen_size = NULL;
   Eina_Bool mobile_version = EINA_FALSE;
   Eina_Bool fullscreen = EINA_FALSE;
   Eina_Bool quit_option = EINA_FALSE;
   Eina_Bool tizen = EINA_FALSE;
   Eina_Bool list = EINA_FALSE;
   Evas_Coord width = WIN_WIDTH, height = WIN_HEIGHT;
   char *widget = NULL;

   Ecore_Getopt_Value values[] = {
     ECORE_GETOPT_VALUE_STR(theme),
     //ECORE_GETOPT_VALUE_BOOL(mobile_version), // TODO: enable in v0.2
     ECORE_GETOPT_VALUE_STR(screen_size),
     ECORE_GETOPT_VALUE_BOOL(fullscreen),
     //ECORE_GETOPT_VALUE_BOOL(tizen), // TODO: enable in v0.2
     ECORE_GETOPT_VALUE_STR(widget),
     ECORE_GETOPT_VALUE_BOOL(list),
     ECORE_GETOPT_VALUE_BOOL(quit_option),
     ECORE_GETOPT_VALUE_BOOL(quit_option),
     ECORE_GETOPT_VALUE_BOOL(quit_option),
     ECORE_GETOPT_VALUE_BOOL(quit_option),
     ECORE_GETOPT_VALUE_NONE
   };

   // for the future support for multi languages
   setlocale(LC_ALL, "");

   elm_init(argc, argv);

   elm_app_compile_bin_dir_set(PACKAGE_BIN_DIR);
   elm_app_compile_data_dir_set(PACKAGE_DATA_DIR);
   elm_app_info_set(main, "elm-theme-viewer", "images/logo.png");

   log_init();

   args = ecore_getopt_parse(&options, values, argc, argv);
   if (args < 0)
     {
        ERR("Could not parse command line options.");
        goto end;
     }

   if (quit_option) goto end;

   // allocate ETV app data
   ed = calloc(1, sizeof(ETV_Data));

   if (theme)
     {
        char path[PATH_MAX];
        if (eina_str_has_extension(theme, ".edj"))
          eina_strlcpy(path, theme, sizeof(path));
        else
          snprintf(path, sizeof(path), "%s.edj", theme);

        edje_file = eina_stringshare_add(path);
     }
   else
     {
        char *path = NULL;
        path = "/usr/share/"DEFAULT_THEME;
        if (!ecore_file_exists(path))
          path = "/usr/local/share/"DEFAULT_THEME;
        edje_file = eina_stringshare_add(path);
     }
   INF("theme file : %s", edje_file);

   // TODO: run this in a background
   theme_init();
   theme_load(edje_file, list);
   if (list) goto list;
   theme_set(edje_file);

   ed->tizen = tizen;
   if (ed->tizen)
     mobile_version = EINA_TRUE;

   if (screen_size)
     {
        width = atoi(strtok(screen_size, "x"));
        height = atoi(strtok(NULL, "x"));
     }
   if (widget)
     ed->start_widget_type = widget_type_get_by_name(widget);

   gui_init_ret = gui_init();
   option_init_ret = option_init();

   gui_version_set(mobile_version);

   if (mobile_version)
     gui_mobile_create(edje_file, width, height, fullscreen);
   else
     gui_create(edje_file, width, height, fullscreen);

   elm_run();

end:
   if (option_init_ret) option_shutdown();
   if (gui_init_ret) gui_shutdown();

   theme_unset(edje_file);

list:
   ETV_SAFE_FREE(ed, free);
   theme_shutdown();
   eina_stringshare_del(edje_file);

   log_shutdown();
   elm_shutdown();

   return 0;
}
