#include <Elementary.h>
#include "common.h"

/*
 * Get the first part from the orig_style
 *
 * h_item/default -> h_item (list)
 * item_compress_odd/default -> item_compress_odd (list)
 */
void
_style_split_1(const char *orig_style, char style[PATH_MAX])
{
   char buf[PATH_MAX] = {0, };
   const char *tok_ptr;

   strncpy(buf, orig_style, sizeof(buf) - 1);
   tok_ptr = strtok(buf, "/");
   strcpy(style, tok_ptr);
   //INF("%s", style);
}

/*
 * Get the second part from the orig_style
 *
 * base/default -> default (actionslider)
 * base/hoversel_horizontal/entry -> hoversel_horizontal (button)
 * item/default/default -> default (genlist item)
 * item/full/default -> full (genlist item)
 */
const char *
_style_split_2(const char *orig_style)
{
   char buf[PATH_MAX] = {0, };
   const char *style;

   strncpy(buf, orig_style, sizeof(buf) - 1);
   strtok(buf, "/");
   style = strtok(NULL, "/");
   //INF("%s", style);

   return style;
}

/* Split the style into two parts
 * FIXME: DOES NOT WORK!
static void
_split_style(const char *style EINA_UNUSED, const char **style1 EINA_UNUSED, const char **style2 EINA_UNUSED)
{
   char buf[PATH_MAX] = {0, };

   strncpy(buf, style, sizeof(buf));
   *style1 = strtok(buf, "/");
   *style2 = strtok(NULL, "/");
}
 */


const char *
widget_style_filter(Widget_Type type, const char *orig_style)
{
   const char *style = NULL;

   switch (type)
     {
      case ETV_ID_ACTIONSLIDER:
      case ETV_ID_BG:
      case ETV_ID_BORDER:
      case ETV_ID_BUTTON:
      case ETV_ID_CALENDAR:
      case ETV_ID_CHECK:
      case ETV_ID_CONFORMANT:
      case ETV_ID_DATETIME:
      case ETV_ID_DAYSELECTOR:
      case ETV_ID_FILESELECTOR:
      case ETV_ID_FILESELECTOR_ENTRY:
      case ETV_ID_FLIPSELECTOR:
      case ETV_ID_FRAME:
      case ETV_ID_HOVER:
      case ETV_ID_LABEL:
      case ETV_ID_PHOTO:
      case ETV_ID_PHOTOCAM:
      case ETV_ID_PLAYER:
      case ETV_ID_POINTER:
      case ETV_ID_POPUP:
      case ETV_ID_RADIO:
      case ETV_ID_SLIDESHOW:
      case ETV_ID_SPINNER:
      case ETV_ID_THUMB:
      case ETV_ID_TOOLTIP:
      case ETV_ID_VIDEO:
         style = orig_style + strlen("base/");
         break;

      case ETV_ID_BUBBLE:
         style = _style_split_2(orig_style);
         break;
      case ETV_ID_CLOCK:
      case ETV_ID_COLORSELECTOR:
      case ETV_ID_CTXPOPUP:
      case ETV_ID_DISKSELECTOR:
      case ETV_ID_ENTRY:
      case ETV_ID_EWS:
      case ETV_ID_FOCUS_HIGHLIGHT:
      case ETV_ID_GENGRID:
      case ETV_ID_GENLIST:
      case ETV_ID_ICON:
      case ETV_ID_INDEX:
      case ETV_ID_LAYOUT:
      case ETV_ID_LIST:
      case ETV_ID_MAP:
      case ETV_ID_MENU:
      case ETV_ID_MULTIBUTTONENTRY:
      case ETV_ID_NAVIFRAME: // item
      case ETV_ID_NOTIFY:
      case ETV_ID_PANEL: // bottom, left, right, top
      case ETV_ID_PANES:
      case ETV_ID_PROGRESSBAR:
      case ETV_ID_SCROLLER:
      case ETV_ID_SEGMENT_CONTROL:
      case ETV_ID_SEPARATOR: // horizontal, vertical
      case ETV_ID_SLIDER: // horizontal/vertical, indicator, popup, ...
      case ETV_ID_TOOLBAR: // base, item, more, object, separator
      case ETV_ID_WIN: // base, inwin
      default:
         //style = _style_split_2(orig_style);
         break;
     }

   if (style)
     return style;
   else
     return orig_style;
}
