#ifndef __INCLUDE_OPTION__
#define __INCLUDE_OPTION__

typedef struct _Option_Data Option_Data;
struct _Option_Data
{
   double option_finger_size;
   double option_scale;
   Eina_Bool option_force_resize;
   Evas_Coord option_size_width;
   Evas_Coord option_size_height;
   Eina_Bool option_disabled;
   Eina_Bool option_focus_highlight;
   Eina_Bool option_focus_highlight_animate;
};

int option_init(void);
void option_shutdown(void);

void option_force_resize_set(Eina_Bool resize);
Eina_Bool option_force_resize_get(void);

void option_disabled_set(Eina_Bool disabled);
Eina_Bool option_disabled_get(void);

void option_scale_set(double scale);
double option_scale_get(void);

void option_size_set(Evas_Coord w, Evas_Coord h);
void option_size_get(Evas_Coord *w, Evas_Coord *h);

void option_focus_highlight_set(Eina_Bool highlight);
Eina_Bool option_focus_highlight_get(void);
void option_focus_highlight_animate_set(Eina_Bool animate);
Eina_Bool option_focus_highlight_animate_get(void);

void option_finger_size_set(double finger_size);
double option_finger_size_get(void);

#endif
