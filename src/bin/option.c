#include <Elementary.h>
#include "common.h"

int _option_init_count = 0;
Option_Data *od = NULL;

#define ETV_CHECK_OPTION \
   if (!od) \
     { \
        CRI("Option is not initialized. Call option_init() before you use it."); \
        return; \
     }

#define ETV_CHECK_OPTION_WITH_VAL(__val) \
   if (!od) \
     { \
        CRI("Option is not initialized. Call option_init() before you use it."); \
        return __val; \
     }


int
option_init(void)
{
   if (!od)
     {
        od = calloc(1, sizeof(Option_Data));
        if (!od)
          {
             CRI("Internal Option_Data is not created!");
             return -1;
          }
        od->option_finger_size = 40;
        od->option_scale = 1.0;
        od->option_size_width = WIDGET_DEFAULT_WIDTH;
        od->option_size_height = WIDGET_DEFAULT_HEIGHT;
     }
   return ++_option_init_count;
}

void
option_shutdown(void)
{
   ETV_CHECK_OPTION;
   if (_option_init_count < 0)
     {
        CRI("option_shutdown() is called more than optioninit() call.");
        return;
     }
   ETV_SAFE_FREE(od, free);
   _option_init_count--;
}

void
option_force_resize_set(Eina_Bool resize)
{
   ETV_CHECK_OPTION;
   od->option_force_resize = !!resize;
}

Eina_Bool
option_force_resize_get(void)
{
   ETV_CHECK_OPTION_WITH_VAL(EINA_FALSE);
   return od->option_force_resize;
}

void
option_disabled_set(Eina_Bool disabled)
{
   ETV_CHECK_OPTION;
   od->option_disabled = !!disabled;
}

Eina_Bool
option_disabled_get(void)
{
   ETV_CHECK_OPTION_WITH_VAL(EINA_FALSE);
   return od->option_disabled;
}

void
option_scale_set(double scale)
{
   ETV_CHECK_OPTION;
   od->option_scale = scale;
}

double
option_scale_get(void)
{
   ETV_CHECK_OPTION_WITH_VAL(0.0);
   return od->option_scale;
}

void
option_size_set(Evas_Coord w, Evas_Coord h)
{
   ETV_CHECK_OPTION;
   if (w >= 0) od->option_size_width = w;
   if (h >= 0) od->option_size_height = h;
}

void
option_size_get(Evas_Coord *w, Evas_Coord *h)
{
   ETV_CHECK_OPTION;
   if (w) *w = od->option_size_width;
   if (h) *h = od->option_size_height;
}

void
option_focus_highlight_set(Eina_Bool highlight)
{
   ETV_CHECK_OPTION;
   od->option_focus_highlight = highlight;
}

Eina_Bool
option_focus_highlight_get(void)
{
   ETV_CHECK_OPTION_WITH_VAL(EINA_FALSE);
   return od->option_focus_highlight;
}

void
option_focus_highlight_animate_set(Eina_Bool animate)
{
   ETV_CHECK_OPTION;
   od->option_focus_highlight_animate = animate;
}

Eina_Bool
option_focus_highlight_animate_get(void)
{
   ETV_CHECK_OPTION_WITH_VAL(EINA_FALSE);
   return od->option_focus_highlight_animate;
}

void
option_finger_size_set(double finger_size)
{
   ETV_CHECK_OPTION;
   od->option_finger_size = finger_size;
}

double
option_finger_size_get(void)
{
   ETV_CHECK_OPTION_WITH_VAL(0.0);
   return od->option_finger_size;
}
