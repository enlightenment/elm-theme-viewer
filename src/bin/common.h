#ifndef __INCLUDE_COMMON_
#define __INCLUDE_COMMON_

#ifdef HAVE_CONFIG_H
#include "elm_theme_viewer_config.h"
#endif

#include "widget.h"

#define WEIGHT evas_object_size_hint_weight_set
#define ALIGN evas_object_size_hint_align_set
#define EXPAND(X) WEIGHT((X), EVAS_HINT_EXPAND, EVAS_HINT_EXPAND)
#define FILL(X) ALIGN((X), EVAS_HINT_FILL, EVAS_HINT_FILL)

#define WIDGET_DEFAULT_WIDTH 250
#define WIDGET_DEFAULT_HEIGHT 150
#define WIDGET_DESC_HEIGHT 100

#define SIZE_WIDTH_MIN 40
#define SIZE_WIDTH_MAX 500
#define SIZE_HEIGHT_MIN 30
#define SIZE_HEIGHT_MAX 400

#define DEFAULT_THEME "elementary/themes/default.edj"

typedef struct _ETV_Data ETV_Data;
struct _ETV_Data
{
   Eina_Bool tizen; // flag to check the Tizen platform
   Widget_Type selected_widget_type; // selected widget type
   Widget_Type start_widget_type; // widget type to start with
};

extern ETV_Data *ed;

#define ETV_SAFE_FREE(_h, _fn) do { if (_h) { _fn((void*)_h); _h = NULL; } } while (0)

#if (ELM_VERSION_MAJOR == 1) && (ELM_VERSION_MINOR < 8)
  #define PANES_TOP "top"
  #define PANES_BOTTOM "bottom"
#else
  #define PANES_TOP "left"
  #define PANES_BOTTOM "right"
#endif

#include "log.h"
#include "theme.h"
#include "widget_style.h"
#include "widget_option.h"
#include "gui.h"
#include "gui_mobile.h"
#include "option_gui.h"
#include "option.h"
#include "util.h"

#endif
