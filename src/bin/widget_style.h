#ifndef __INCLUDE_WIDGET_STYLE__
#define __INCLUDE_WIDGET_STYLE__

typedef struct _Style_Data Style_Data;
struct _Style_Data
{
   Widget_Type widget_type;
   Widget_Data_Style *wds;
};

void _style_split_1(const char *orig_style, char style[PATH_MAX]);
const char * _style_split_2(const char *orig_style);
const char * widget_style_filter(Widget_Type type, const char *orig_style);

#endif
