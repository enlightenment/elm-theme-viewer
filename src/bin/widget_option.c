#include <Elementary.h>
#include "common.h"

Widget_Option_Data *wod = NULL;
extern Gui_Data *gd;

#define WIDGET_OPTION_CHECK_ADD(text__, val__, cb__) \
   o = elm_check_add(box); \
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o); \
   elm_object_text_set(o, text__); \
   elm_check_state_set(o, val__); \
   elm_box_pack_end(box, o); \
   evas_object_show(o); \
   evas_object_smart_callback_add(o, "changed", cb__, NULL);

#define WIDGET_OPTION_BUTTON_ADD(text__, cb__) \
   o = elm_button_add(box); \
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o); \
   elm_object_text_set(o, text__); \
   elm_box_pack_end(box, o); \
   evas_object_show(o); \
   evas_object_smart_callback_add(o, "clicked", cb__, NULL);

#define WIDGET_OPTION_RADIO_ADD(text__, value__, cb__) \
   o = elm_radio_add(box); \
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o); \
   elm_object_text_set(o, text__); \
   elm_radio_state_value_set(o, value__); \
   elm_box_pack_end(box, o); \
   evas_object_show(o); \
   evas_object_smart_callback_add(o, "changed", cb__, NULL);

#define WIDGET_OPTION_LABEL_ADD(text__) \
   o = elm_label_add(box); \
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); ALIGN(o, 0.0, 0.5); \
   elm_object_text_set(o, text__); \
   elm_box_pack_end(box, o); \
   evas_object_show(o);

#define WIDGET_OPTION_SEPARATOR_ADD() \
   o = elm_separator_add(box); \
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); ALIGN(o, EVAS_HINT_FILL, 0.0); \
   elm_separator_horizontal_set(o, EINA_TRUE); \
   elm_box_pack_end(box, o); \
   evas_object_show(o);

#define WIDGET_OPTION_HORIZ_PADDING_ADD() \
   o = elm_box_add(box); \
   WEIGHT(o, 0.0, EVAS_HINT_EXPAND; ALIGN(o, 0.0, EVAS_HINT_FILL); \
   evas_object_size_hint_min_set(o, elm_config_scale_get() * 10, 10); \
   elm_box_pack_end(box, o); \
   evas_object_show(o);

#define WIDGET_OPTION_VERT_PADDING_ADD() \
   o = elm_box_add(box); \
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); ALIGN(o, EVAS_HINT_FILL, 0.0); \
   evas_object_size_hint_min_set(o, 0, elm_config_scale_get() * 10); \
   elm_box_pack_end(box, o); \
   evas_object_show(o);

#define WIDGET_OPTION_PADDING_ADD(parent) \
   o = elm_box_add(parent); \
   EXPAND(o); FILL(o); \
   elm_box_pack_end(parent, o); \
   evas_object_show(o);

#define WIDGET_OPTION_HORIZ_BOX_ADD() \
   bx = o = elm_box_add(box); \
   elm_box_horizontal_set(o, EINA_TRUE); \
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o); \
   elm_box_pack_end(box, o); \
   evas_object_show(o);

#define WIDGET_OPTION_HOVERSEL_ADD() \
   o = elm_hoversel_add(box); \
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); ALIGN(o, 0.0, 0.0); \
   elm_hoversel_hover_parent_set(o, gd->win); \
   elm_box_pack_end(box, o); \
   evas_object_show(o);

#define WIDGET_OPTION_HOVERSEL_FULL_ADD(var__, val__, cb__) \
   WIDGET_OPTION_HOVERSEL_ADD(); \
   elm_object_text_set(o, var__[val__]); \
   for (i = 0; var__[i]; i++) \
     { \
        elm_hoversel_item_add(o, var__[i], NULL, ELM_ICON_NONE, \
                              cb__, \
                              (void *)(uintptr_t)i); \
     }

// common
#define WIDGET_OPTION_CHECK_CHANGED_CB(name__, val__) \
   static void \
   _##name__##_changed_cb(void *data EINA_UNUSED, Evas_Object *obj, \
                          void *event_info EINA_UNUSED) \
   { \
      val__ = elm_check_state_get(obj); \
              gui_preview_update(); \
   }
#define WIDGET_OPTION_HOVERSEL_CHANGED_CB(cb__, var__, val__) \
   static void \
   cb__(void *data, Evas_Object *obj, \
        void *event_info EINA_UNUSED) \
   { \
      val__ = (int)(uintptr_t)data; \
      gui_preview_update(); \
   \
      elm_object_text_set(obj, var__[val__]); \
   }

#define WIDGET_OPTION_FOCUS_BUTTON_CLICKED_CB(cb__) \
   static void \
   cb__(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, \
        void *event_info EINA_UNUSED) \
   { \
      gui_preview_focus(); \
   }

const char *_text_len[] =
{
   "Short",
   "Middle",
   "Long",
   NULL
};

const char *_select_mode[] =
{
   "DEFAULT",
   "ALWAYS",
   "NONE",
   "DISPLAY_ONLY",
   NULL
};

// actionslider
WIDGET_OPTION_CHECK_CHANGED_CB(actslider_magnet_pos_left, wod->actslider_magnet_pos_left);
WIDGET_OPTION_CHECK_CHANGED_CB(actslider_magnet_pos_center, wod->actslider_magnet_pos_center);
WIDGET_OPTION_CHECK_CHANGED_CB(actslider_magnet_pos_right, wod->actslider_magnet_pos_right);

const char *_actionslider_pos[] =
{
   "Left",
   "Center",
   "Right",
   NULL
};

WIDGET_OPTION_HOVERSEL_CHANGED_CB(_actionslider_indi_pos_changed_cb,
                                  _actionslider_pos, wod->actslider_indi_pos);

static void
_widget_option_actionslider_create(Evas_Object *box)
{
   Evas_Object *o = NULL;
   int i = 0;

   // magnet pos
   WIDGET_OPTION_LABEL_ADD(" &lt; Magnet Position &gt; ");
   WIDGET_OPTION_CHECK_ADD("Left", wod->actslider_magnet_pos_left,
                           _actslider_magnet_pos_left_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Center", wod->actslider_magnet_pos_center,
                           _actslider_magnet_pos_center_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Right", wod->actslider_magnet_pos_right,
                           _actslider_magnet_pos_right_changed_cb);

   // indicator pos
   WIDGET_OPTION_LABEL_ADD(" &lt; Indicator Position &gt; ");
   WIDGET_OPTION_HOVERSEL_FULL_ADD(_actionslider_pos, wod->actslider_indi_pos,
                                   _actionslider_indi_pos_changed_cb);
}

// button
WIDGET_OPTION_CHECK_CHANGED_CB(btn_text, wod->btn_text);
WIDGET_OPTION_CHECK_CHANGED_CB(btn_image, wod->btn_image);
WIDGET_OPTION_FOCUS_BUTTON_CLICKED_CB(_btn_focus_clicked_cb);

static void
_widget_option_button_create(Evas_Object *box)
{
   Evas_Object *o = NULL;

   WIDGET_OPTION_CHECK_ADD("Text", wod->btn_text, _btn_text_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Image", wod->btn_image, _btn_image_changed_cb);
   WIDGET_OPTION_BUTTON_ADD("Set Focus", _btn_focus_clicked_cb);
}

// check
WIDGET_OPTION_CHECK_CHANGED_CB(chk_selected, wod->chk_selected);
WIDGET_OPTION_CHECK_CHANGED_CB(chk_text, wod->chk_text);
WIDGET_OPTION_CHECK_CHANGED_CB(chk_image, wod->chk_image);

static void
_widget_option_check_create(Evas_Object *box)
{
   Evas_Object *o = NULL;

   WIDGET_OPTION_CHECK_ADD("Selected", wod->chk_selected,
                           _chk_selected_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Text", wod->chk_text, _chk_text_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Image", wod->chk_image, _chk_image_changed_cb);
}

// datetime
WIDGET_OPTION_CHECK_CHANGED_CB(datetime_year, wod->dt_year);
WIDGET_OPTION_CHECK_CHANGED_CB(datetime_month, wod->dt_month);
WIDGET_OPTION_CHECK_CHANGED_CB(datetime_date, wod->dt_date);
WIDGET_OPTION_CHECK_CHANGED_CB(datetime_hour, wod->dt_hour);
WIDGET_OPTION_CHECK_CHANGED_CB(datetime_minute, wod->dt_minute);
WIDGET_OPTION_CHECK_CHANGED_CB(datetime_ampm, wod->dt_ampm);

static void
_widget_option_datetime_create(Evas_Object *box)
{
   Evas_Object *o = NULL;

   WIDGET_OPTION_CHECK_ADD("Year", wod->dt_year,
                           _datetime_year_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Month", wod->dt_month,
                           _datetime_month_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Date", wod->dt_date,
                           _datetime_date_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Hour", wod->dt_hour,
                           _datetime_hour_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Minute", wod->dt_minute,
                           _datetime_minute_changed_cb);
   WIDGET_OPTION_CHECK_ADD("AMPM", wod->dt_ampm,
                           _datetime_ampm_changed_cb);
}


// entry
WIDGET_OPTION_HOVERSEL_CHANGED_CB(_entry_text_len_changed_cb,
                                  _text_len, wod->entry_text_len);
WIDGET_OPTION_CHECK_CHANGED_CB(entry_guide_text, wod->entry_guide_text);

static void
_widget_option_entry_create(Evas_Object *box)
{
   Evas_Object *o = NULL;
   int i = 0;

   // text length
   WIDGET_OPTION_LABEL_ADD(" &lt; Text Length &gt; ");
   WIDGET_OPTION_HOVERSEL_FULL_ADD(_text_len, wod->entry_text_len,
                                   _entry_text_len_changed_cb);

   WIDGET_OPTION_SEPARATOR_ADD();

   WIDGET_OPTION_CHECK_ADD("Guide Text", wod->entry_guide_text,
                           _entry_guide_text_changed_cb);
}

// gengrid
Evas_Object *gengrid_width_entry = NULL;
Evas_Object *gengrid_height_entry = NULL;

static void
_gengrid_item_size_changed_cb(void *data EINA_UNUSED,
                              Evas_Object *obj EINA_UNUSED,
                              void *event_info EINA_UNUSED)
{
   wod->gengrid_item_width = atoi(elm_entry_entry_get(gengrid_width_entry));
   wod->gengrid_item_height = atoi(elm_entry_entry_get(gengrid_height_entry));
   gui_preview_update();
}

WIDGET_OPTION_HOVERSEL_CHANGED_CB(_gengrid_text_len_changed_cb,
                                  _text_len, wod->gengrid_text_len);
WIDGET_OPTION_HOVERSEL_CHANGED_CB(_gengrid_select_mode_changed_cb,
                                  _select_mode, wod->gengrid_select_mode);

static void
_gengrid_entry_focused_cb(void *data EINA_UNUSED, Evas_Object *obj,
                          void *event_info EINA_UNUSED)
{
   elm_entry_select_all(obj);
}

static void
_gengrid_horz_align_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                               void *event_info EINA_UNUSED)
{
   wod->gengrid_horz_align = elm_slider_value_get(obj);
   gui_preview_update();
}

static void
_gengrid_vert_align_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                               void *event_info EINA_UNUSED)
{
   wod->gengrid_vert_align = elm_slider_value_get(obj);
   gui_preview_update();
}

static void
_widget_option_gengrid_create(Evas_Object *box)
{
   Evas_Object *o = NULL, *bx = NULL;
   char buf[PATH_MAX] = { 0 };
   int i = 0;
   //struct _Gengrid_Option_Data data = { 0 };

   WIDGET_OPTION_LABEL_ADD(" &lt; Gengrid Item Size &gt; ");

   // width
   WIDGET_OPTION_HORIZ_BOX_ADD();

   o = elm_label_add(bx);
   WEIGHT(o, 0.0, EVAS_HINT_EXPAND); FILL(o);
   elm_object_text_set(o, "Width : ");
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   gengrid_width_entry = o = elm_entry_add(bx);
   EXPAND(o); FILL(o);
   elm_entry_single_line_set(o, EINA_TRUE);
   elm_entry_scrollable_set(o, EINA_TRUE);
   snprintf(buf, PATH_MAX, "%d", wod->gengrid_item_width);
   elm_entry_entry_set(o, buf);
   evas_object_smart_callback_add(o, "focused", _gengrid_entry_focused_cb, NULL);
   evas_object_smart_callback_add(o, "activated",
                                  _gengrid_item_size_changed_cb, NULL);
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   // height
   WIDGET_OPTION_HORIZ_BOX_ADD();

   o = elm_label_add(bx);
   WEIGHT(o, 0.0, 0.0); FILL(o);
   elm_object_text_set(o, "Height : ");
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   gengrid_height_entry = o = elm_entry_add(bx);
   EXPAND(o); FILL(o);
   elm_entry_single_line_set(o, EINA_TRUE);
   elm_entry_scrollable_set(o, EINA_TRUE);
   snprintf(buf, PATH_MAX, "%d", wod->gengrid_item_height);
   elm_entry_entry_set(o, buf);
   evas_object_smart_callback_add(o, "focused", _gengrid_entry_focused_cb, NULL);
   evas_object_smart_callback_add(o, "activated",
                                  _gengrid_item_size_changed_cb, NULL);
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   o = elm_button_add(box);
   ALIGN(o, 0.0, EVAS_HINT_FILL);
   elm_object_text_set(o, "Apply Item Size Change");
   elm_box_pack_end(box, o);
   evas_object_show(o);
   evas_object_smart_callback_add(o, "clicked",
                                  _gengrid_item_size_changed_cb, NULL);

   WIDGET_OPTION_SEPARATOR_ADD();

   // text length
   WIDGET_OPTION_LABEL_ADD(" &lt; Text Length &gt; ");
   WIDGET_OPTION_HOVERSEL_FULL_ADD(_text_len, wod->gengrid_text_len,
                                   _gengrid_text_len_changed_cb);

   WIDGET_OPTION_SEPARATOR_ADD();

   // horizontal align
   WIDGET_OPTION_HORIZ_BOX_ADD();

   o = elm_label_add(box);
   elm_object_text_set(o, "Horizontal Alignment : ");
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   o = elm_slider_add(bx);
   elm_slider_min_max_set(o, 0.0, 1.0);
   elm_slider_unit_format_set(o, "%1.1f");
   elm_slider_indicator_format_set(o, "%1.1f");
   elm_slider_value_set(o, wod->gengrid_horz_align);
   elm_slider_span_size_set(o, elm_config_scale_get() * 100);
   evas_object_smart_callback_add(o, "delay,changed",
                                  _gengrid_horz_align_changed_cb, NULL);
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   WIDGET_OPTION_PADDING_ADD(bx);

   // vert align
   WIDGET_OPTION_HORIZ_BOX_ADD();

   o = elm_label_add(box);
   elm_object_text_set(o, "Vertical Alignment : ");
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   o = elm_slider_add(bx);
   elm_slider_min_max_set(o, 0.0, 1.0);
   elm_slider_unit_format_set(o, "%1.1f");
   elm_slider_indicator_format_set(o, "%1.1f");
   elm_slider_value_set(o, wod->gengrid_vert_align);
   elm_slider_span_size_set(o, elm_config_scale_get() * 100);
   evas_object_smart_callback_add(o, "delay,changed",
                                  _gengrid_vert_align_changed_cb, NULL);
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   WIDGET_OPTION_SEPARATOR_ADD();

   // select mode
   WIDGET_OPTION_LABEL_ADD(" &lt; Select Mode &gt; ");
   WIDGET_OPTION_HOVERSEL_FULL_ADD(_select_mode, wod->gengrid_select_mode,
                                   _gengrid_select_mode_changed_cb);
   WIDGET_OPTION_PADDING_ADD(bx);
}

// genlist
WIDGET_OPTION_HOVERSEL_CHANGED_CB(_genlist_text_len_changed_cb,
                                  _text_len, wod->genlist_text_len);
WIDGET_OPTION_HOVERSEL_CHANGED_CB(_genlist_select_mode_changed_cb,
                                  _select_mode, wod->genlist_select_mode);

static void
_genlist_content_type_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                                 void *event_info EINA_UNUSED)
{
   wod->genlist_content_type = elm_radio_value_get(obj);
   gui_preview_update();
}

static void
_widget_option_genlist_create(Evas_Object *box)
{
   Evas_Object *o = NULL, *rdg = NULL;
   int i = 0;

   // text length
   WIDGET_OPTION_LABEL_ADD(" &lt; Text Length &gt; ");
   WIDGET_OPTION_HOVERSEL_FULL_ADD(_text_len, wod->genlist_text_len,
                                   _genlist_text_len_changed_cb);

   WIDGET_OPTION_SEPARATOR_ADD();

   // content type
   WIDGET_OPTION_LABEL_ADD(" &lt; Content Type &gt; ");
   WIDGET_OPTION_RADIO_ADD("Icon", 0, _genlist_content_type_changed_cb);
   rdg = o;
   WIDGET_OPTION_RADIO_ADD("Check", 1, _genlist_content_type_changed_cb);
   elm_radio_group_add(o, rdg);
   WIDGET_OPTION_RADIO_ADD("Button", 2, _genlist_content_type_changed_cb);
   elm_radio_group_add(o, rdg);
   elm_radio_value_set(rdg, wod->genlist_content_type);

   WIDGET_OPTION_SEPARATOR_ADD();

   // select mode
   WIDGET_OPTION_LABEL_ADD(" &lt; Select Mode &gt; ");
   WIDGET_OPTION_HOVERSEL_FULL_ADD(_select_mode, wod->genlist_select_mode,
                                   _genlist_select_mode_changed_cb);
}

// notify
WIDGET_OPTION_CHECK_CHANGED_CB(notify_event, wod->notify_event);

static void
_notify_horz_align_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                              void *event_info EINA_UNUSED)
{
   wod->notify_horz_align = elm_slider_value_get(obj);
}

static void
_notify_vert_align_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                              void *event_info EINA_UNUSED)
{
   wod->notify_vert_align = elm_slider_value_get(obj);
}

static void
_widget_option_notify_create(Evas_Object *box)
{
   Evas_Object *o = NULL, *bx = NULL;

   WIDGET_OPTION_CHECK_ADD("Allow background events", wod->notify_event, _notify_event_changed_cb);

   // horizontal align
   WIDGET_OPTION_HORIZ_BOX_ADD();

   o = elm_label_add(bx);
   elm_object_text_set(o, "Horizontal Alignment : ");
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   o = elm_slider_add(bx);
   elm_slider_min_max_set(o, 0.0, 1.0);
   elm_slider_unit_format_set(o, "%1.1f");
   elm_slider_indicator_format_set(o, "%1.1f");
   elm_slider_value_set(o, wod->gengrid_horz_align);
   elm_slider_span_size_set(o, elm_config_scale_get() * 100);
   evas_object_smart_callback_add(o, "delay,changed",
                                  _notify_horz_align_changed_cb, NULL);
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   WIDGET_OPTION_PADDING_ADD(bx);

   // vert align
   WIDGET_OPTION_HORIZ_BOX_ADD();

   o = elm_label_add(bx);
   elm_object_text_set(o, "Vertical Alignment : ");
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   o = elm_slider_add(bx);
   elm_slider_min_max_set(o, 0.0, 1.0);
   elm_slider_unit_format_set(o, "%1.1f");
   elm_slider_indicator_format_set(o, "%1.1f");
   elm_slider_value_set(o, wod->gengrid_vert_align);
   elm_slider_span_size_set(o, elm_config_scale_get() * 100);
   evas_object_smart_callback_add(o, "delay,changed",
                                  _notify_vert_align_changed_cb, NULL);
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   WIDGET_OPTION_PADDING_ADD(bx);
}

// popup
const char *_popup_content_type[] =
{
   "Short Text as content",
   "Long Text as content",
   "Button as content",
   "200x50 Rectangle as content",
   "500x200 Rectangle as content",
   NULL
};

struct _Popup_Orient
{
   const char *name;
   Elm_Popup_Orient orient;
};

struct _Popup_Orient _popup_orient[ELM_POPUP_ORIENT_LAST] =
{
     { "Top", ELM_POPUP_ORIENT_TOP },
     { "Center", ELM_POPUP_ORIENT_CENTER },
     { "Bottom", ELM_POPUP_ORIENT_BOTTOM },
     { "Left", ELM_POPUP_ORIENT_LEFT },
     { "Right", ELM_POPUP_ORIENT_RIGHT },
     { "Top Left", ELM_POPUP_ORIENT_TOP_LEFT },
     { "Top Right", ELM_POPUP_ORIENT_TOP_RIGHT },
     { "Bottom Left", ELM_POPUP_ORIENT_BOTTOM_LEFT },
     { "Bottom Right", ELM_POPUP_ORIENT_BOTTOM_RIGHT }
};

const char *_popup_button[] =
{
   "0 Button",
   "1 Button",
   "2 Buttons",
   "3 Buttons",
   NULL
};

WIDGET_OPTION_HOVERSEL_CHANGED_CB(_popup_button_changed_cb, _popup_button,
                                  wod->pop_button);
WIDGET_OPTION_HOVERSEL_CHANGED_CB(_popup_content_type_changed_cb, _popup_content_type,
                                  wod->pop_content_type);
WIDGET_OPTION_CHECK_CHANGED_CB(pop_title_text, wod->pop_title_text);
WIDGET_OPTION_CHECK_CHANGED_CB(pop_title_icon, wod->pop_title_icon);

static void
_popup_orient_changed_cb(void *data, Evas_Object *obj,
                         void *event_info EINA_UNUSED)
{
   wod->pop_orient = (int)(uintptr_t)data;
   gui_preview_update();

   elm_object_text_set(obj, _popup_orient[wod->pop_orient].name);
}

static void
_widget_option_popup_create(Evas_Object *box)
{
   Evas_Object *o = NULL;
   int i = 0;

   // number of buttons
   WIDGET_OPTION_LABEL_ADD(" &lt; Number of Buttons &gt; ");
   WIDGET_OPTION_HOVERSEL_FULL_ADD(_popup_button, wod->pop_button,
                                   _popup_button_changed_cb);

   WIDGET_OPTION_SEPARATOR_ADD();

   // title optoin
   WIDGET_OPTION_LABEL_ADD(" &lt; Title Option &gt; ");
   WIDGET_OPTION_CHECK_ADD("Title Text", wod->pop_title_text,
                           _pop_title_text_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Title Icon", wod->pop_title_icon,
                           _pop_title_icon_changed_cb);

   WIDGET_OPTION_SEPARATOR_ADD();

   // content option
   WIDGET_OPTION_LABEL_ADD(" &lt; Content Option &gt; ");
   WIDGET_OPTION_HOVERSEL_FULL_ADD(_popup_content_type, wod->pop_content_type,
                                   _popup_content_type_changed_cb);

   WIDGET_OPTION_SEPARATOR_ADD();

   // orient set
   WIDGET_OPTION_LABEL_ADD(" &lt; Popup Orient &gt; ");
   WIDGET_OPTION_HOVERSEL_ADD();
   elm_object_text_set(o, _popup_orient[wod->pop_orient].name);
   for (i = 0; i < ELM_POPUP_ORIENT_LAST; i++)
     {
        elm_hoversel_item_add(o, _popup_orient[i].name, NULL, ELM_ICON_NONE,
                              _popup_orient_changed_cb,
                              (void *)(uintptr_t)_popup_orient[i].orient);
     }
}

// progressbar
static void
_progressbar_pulse_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                              void *event_info EINA_UNUSED)
{
   wod->pg_pulse = elm_radio_value_get(obj);
   gui_preview_update();
}

static void
_widget_option_progressbar_create(Evas_Object *box)
{
   Evas_Object *o = NULL, *rdg = NULL;

   rdg = o = elm_radio_add(box);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_object_text_set(o, "Pulse");
   elm_radio_state_value_set(o, 1);
   elm_box_pack_end(box, o);
   evas_object_show(o);
   evas_object_smart_callback_add(o, "changed",
                                  _progressbar_pulse_changed_cb, NULL);

   o = elm_radio_add(box);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_object_text_set(o, "Progress");
   elm_radio_state_value_set(o, 0);
   elm_radio_group_add(o, rdg);
   elm_box_pack_end(box, o);
   evas_object_show(o);
   evas_object_smart_callback_add(o, "changed",
                                  _progressbar_pulse_changed_cb, NULL);

   elm_radio_value_set(rdg, wod->pg_pulse);
}

// radio
WIDGET_OPTION_CHECK_CHANGED_CB(rd_text, wod->rd_text);
WIDGET_OPTION_CHECK_CHANGED_CB(rd_image, wod->rd_image);

static void
_widget_option_radio_create(Evas_Object *box)
{
   Evas_Object *o = NULL;

   WIDGET_OPTION_CHECK_ADD("Text", wod->rd_text, _rd_text_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Image", wod->rd_image, _rd_image_changed_cb);
}

// slider
WIDGET_OPTION_CHECK_CHANGED_CB(sl_text, wod->sl_text);
WIDGET_OPTION_CHECK_CHANGED_CB(sl_image, wod->sl_image);
WIDGET_OPTION_CHECK_CHANGED_CB(sl_indicator, wod->sl_indicator);

static void
_widget_option_slider_create(Evas_Object *box)
{
   Evas_Object *o = NULL;

   WIDGET_OPTION_CHECK_ADD("Text", wod->sl_text, _sl_text_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Image", wod->sl_image, _sl_image_changed_cb);
   WIDGET_OPTION_CHECK_ADD("Indicator", wod->sl_indicator,
                           _sl_indicator_changed_cb);
}

// toolbar
const char *_toolbar_shrink[] =
{
   "None",
   "Hide",
   "Scroll",
   "Menu",
   "Expand",
   NULL
};

WIDGET_OPTION_HOVERSEL_CHANGED_CB(_toolbar_shrink_changed_cb,
                                  _toolbar_shrink, wod->tb_shrink);

static void
_widget_option_toolbar_create(Evas_Object *box)
{
   Evas_Object *o = NULL;
   int i = 0;

   WIDGET_OPTION_LABEL_ADD(" &lt; Shrink Mode &gt; ");
   WIDGET_OPTION_HOVERSEL_FULL_ADD(_toolbar_shrink, wod->tb_shrink,
                                   _toolbar_shrink_changed_cb);
}

// not implemented
static void
_widget_option_not_implemented_create(Evas_Object *box)
{
   Evas_Object *o = NULL;

   o = elm_label_add(box);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_label_line_wrap_set(o, ELM_WRAP_MIXED);
   elm_object_text_set(o, "Widget specific option is available here. But this widget does not support that feature now. Sorry.");
   elm_box_pack_end(box, o);
   evas_object_show(o);
}

static void
_wod_init(void)
{
   wod = calloc(1, sizeof(Widget_Option_Data));
   if (!wod)
     {
        CRI("Internal Widget_Option_Data is not created!");
        return;
     }
   wod->actslider_magnet_pos_center = EINA_TRUE;
   wod->actslider_magnet_pos_right = EINA_TRUE;
   wod->actslider_indi_pos = 0;
   wod->btn_text = EINA_TRUE;
   wod->chk_text = EINA_TRUE;
   wod->dt_year = EINA_TRUE;
   wod->dt_month = EINA_TRUE;
   wod->dt_date = EINA_TRUE;
   wod->dt_hour = EINA_TRUE;
   wod->dt_minute = EINA_TRUE;
   wod->dt_ampm = EINA_TRUE;
   wod->entry_text_len = 2;
   wod->gengrid_item_width = 168;
   wod->gengrid_item_height = 168;
   wod->genlist_text_len = 2;
   wod->pop_button = 2;
   wod->pop_title_text = 1;
   wod->pop_orient = 1;
   wod->pg_pulse = EINA_TRUE;
   wod->rd_text = EINA_TRUE;
   wod->sl_text = EINA_TRUE;
   wod->sl_indicator = EINA_TRUE;
}

void
widget_option_content_update(Widget_Type type)
{
   Evas_Object *o = NULL, *fr = NULL, *box = NULL;
   char buf[PATH_MAX] = { 0 };
   static Widget_Type prev_type = ETV_ID_NONE;

   if (!gd) return;
   if (!wod) _wod_init();

   // optimization. do not create the same object again.
   if (prev_type == type) return;
   prev_type = type;

   // option padding frame
   fr = o = elm_frame_add(gd->widget_option_scr);
   EXPAND(o); FILL(o);
   elm_object_style_set(o, "pad_medium");
   evas_object_show(o);

   // option box
   box = o = elm_box_add(fr);
   EXPAND(o); FILL(o);
   elm_box_padding_set(o, 0, elm_config_scale_get() * 5);
   elm_object_content_set(fr, o);
   evas_object_show(o);

   // widget description label
   o = elm_label_add(box);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_label_line_wrap_set(o, ELM_WRAP_MIXED);
   sprintf(buf, "< %s widget specific options >", widget_name_get_by_type(type));
   elm_object_text_set(o, buf);
   elm_box_pack_end(box, o);
   evas_object_show(o);

#define WIDGET_OPTION(name__, type__) \
   else if (type == type__) \
     _widget_option_##name__##_create(box); \

   if (type == ETV_ID_NONE)
     {
        o = elm_object_content_unset(gd->widget_option_scr);
        if (o) evas_object_del(o);
        return;
     }
   WIDGET_OPTION(actionslider, ETV_ID_ACTIONSLIDER)
   WIDGET_OPTION(button, ETV_ID_BUTTON)
   WIDGET_OPTION(check, ETV_ID_CHECK)
   WIDGET_OPTION(datetime, ETV_ID_DATETIME)
   WIDGET_OPTION(entry, ETV_ID_ENTRY)
   WIDGET_OPTION(gengrid, ETV_ID_GENGRID)
   WIDGET_OPTION(genlist, ETV_ID_GENLIST)
   WIDGET_OPTION(notify, ETV_ID_NOTIFY)
   WIDGET_OPTION(popup, ETV_ID_POPUP)
   WIDGET_OPTION(progressbar, ETV_ID_PROGRESSBAR)
   WIDGET_OPTION(radio, ETV_ID_RADIO)
   WIDGET_OPTION(slider, ETV_ID_SLIDER)
   WIDGET_OPTION(toolbar, ETV_ID_TOOLBAR)
   else
     _widget_option_not_implemented_create(box);

   // padding
   o = elm_box_add(box);
   EXPAND(o); FILL(o);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   // set the frame into option scroller
   elm_object_content_set(gd->widget_option_scr, fr);
}
