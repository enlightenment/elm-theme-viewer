#include <Elementary.h>
#include "common.h"

Option_Gui_Data *ogd = NULL;
extern Gui_Data *gd;

static void
_force_resize_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                         void *event_info EINA_UNUSED)
{
   option_force_resize_set(elm_check_state_get(obj));

   elm_object_disabled_set(ogd->size_width_slider, !option_force_resize_get());
   elm_object_disabled_set(ogd->size_height_slider, !option_force_resize_get());
}

void
option_gui_force_resize_create(Evas_Object *box)
{
   Evas_Object *o;

   ogd->option_force_resize_prev = option_force_resize_get();

   o = elm_check_add(box);
   elm_object_text_set(o, "Force resize");
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   elm_check_state_set(o, option_force_resize_get());
   evas_object_smart_callback_add(o, "changed", _force_resize_changed_cb, NULL);
   elm_box_pack_end(box, o);
   evas_object_show(o);
}

static void
_size_width_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                       void *event_info EINA_UNUSED)
{
   double val = elm_slider_value_get(obj);
   double v;

   v = round(val);
   if (v != val) elm_slider_value_set(obj, v);
   option_size_set(v, -1);

   INF("size width changed : %f %f", val, v);
}

void
option_gui_width_size_create(Evas_Object *box)
{
   Evas_Object *o;
   Evas_Coord w = 0;

   option_size_get(&ogd->option_size_width_prev, NULL);

   o = elm_label_add(box);
   elm_object_text_set(o, "Size Width");
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   ogd->size_width_slider = o = elm_slider_add(box);
   elm_slider_unit_format_set(o, "%1.0f");
   elm_slider_indicator_format_set(o, "%1.0f");
   elm_slider_min_max_set(o, SIZE_WIDTH_MIN, SIZE_WIDTH_MAX);
   option_size_get(&w, NULL);
   elm_slider_value_set(o, w);
   elm_object_disabled_set(o, !option_force_resize_get());
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   evas_object_smart_callback_add(o, "delay,changed", _size_width_changed_cb,
                                  NULL);
   elm_box_pack_end(box, o);
   evas_object_show(o);
}

static void
_size_height_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                        void *event_info EINA_UNUSED)
{
   double val = elm_slider_value_get(obj);
   double v;

   v = round(val);
   if (v != val) elm_slider_value_set(obj, v);
   option_size_set(-1, v);

   INF("size height changed : %f %f", val, v);
}

void
option_gui_height_size_create(Evas_Object *box)
{
   Evas_Object *o;
   Evas_Coord h = 0;

   option_size_get(NULL, &ogd->option_size_height_prev);

   o = elm_label_add(box);
   elm_object_text_set(o, "Size Height");
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   ogd->size_height_slider = o = elm_slider_add(box);
   elm_slider_unit_format_set(o, "%1.0f");
   elm_slider_indicator_format_set(o, "%1.0f");
   elm_slider_min_max_set(o, SIZE_HEIGHT_MIN, SIZE_HEIGHT_MAX);
   option_size_get(NULL, &h);
   elm_slider_value_set(o, h);
   elm_object_disabled_set(o, !option_force_resize_get());
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   evas_object_smart_callback_add(o, "delay,changed", _size_height_changed_cb,
                                  NULL);
   elm_box_pack_end(box, o);
   evas_object_show(o);
}

static void
_disabled_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                     void *event_info EINA_UNUSED)
{
   option_disabled_set(elm_check_state_get(obj));

   if (gd->preview_obj)
     elm_object_disabled_set(gd->preview_obj, option_disabled_get());
}

void
option_gui_disabled_create(Evas_Object *box)
{
   Evas_Object *o = NULL;

   o = elm_check_add(box);
   elm_object_text_set(o, "Disabled");
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   elm_check_state_set(o, option_disabled_get());
   evas_object_smart_callback_add(o, "changed", _disabled_changed_cb, NULL);
   elm_box_pack_end(box, o);
   evas_object_show(o);
}

static void
_focus_highlight_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                            void *event_info EINA_UNUSED)
{
   option_focus_highlight_set(elm_check_state_get(obj));
}

void
option_gui_focus_highlight_create(Evas_Object *box)
{
   Evas_Object *o = NULL;
   o = elm_check_add(box);
   elm_object_text_set(o, "Focus Highlight");
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   elm_check_state_set(o, option_focus_highlight_get());
   evas_object_smart_callback_add(o, "changed",
                                  _focus_highlight_changed_cb, NULL);
   elm_box_pack_end(box, o);
   evas_object_show(o);
}

static void
_focus_highlight_animate_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                                    void *event_info EINA_UNUSED)
{
   option_focus_highlight_animate_set(elm_check_state_get(obj));
}

void
option_gui_focus_highlight_animate_create(Evas_Object *box)
{
   Evas_Object *o = NULL;
   o = elm_check_add(box);
   elm_object_text_set(o, "Focus Highlight Animation");
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   elm_check_state_set(o, option_focus_highlight_animate_get());
   evas_object_smart_callback_add(o, "changed",
                                  _focus_highlight_animate_changed_cb, NULL);
   elm_box_pack_end(box, o);
   evas_object_show(o);
}

static void
_custom_color_mouse_up_cb(void *data EINA_UNUSED, Evas *evas EINA_UNUSED,
                          Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Evas_Event_Mouse_Up *ev = event_info;

   if (ev->button !=1) return;
   if (ev->event_flags & EVAS_EVENT_FLAG_ON_HOLD) return;

   // TODO : return when the default background is used.

   printf("%s\n", __func__);
}

static void
_background_changed_cb(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED,
                       void *event_info EINA_UNUSED)
{
   printf("background changed\n");
}

void
option_gui_background_create(Evas_Object *box)
{
   Evas_Object *o = NULL, *bx = NULL, *rdg = NULL;

   o = elm_label_add(box);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_object_text_set(o, "Background Setting");
   elm_box_pack_end(box, o);
   evas_object_show(o);

   bx = o = elm_box_add(box);
   EXPAND(o); FILL(o);
   elm_box_horizontal_set(o, EINA_TRUE);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   o = rdg = elm_radio_add(bx);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_object_text_set(o, "Default Background");
   elm_radio_state_value_set(o, 1);
   evas_object_smart_callback_add(o, "changed",
                                  _background_changed_cb, NULL);
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   o = elm_radio_add(bx);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_object_text_set(o, "Theme Background");
   elm_radio_state_value_set(o, 2);
   elm_radio_group_add(o, rdg);
   evas_object_smart_callback_add(o, "changed",
                                  _background_changed_cb, NULL);
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   bx = o = elm_box_add(box);
   EXPAND(o); FILL(o);
   elm_box_horizontal_set(o, EINA_TRUE);
   elm_box_padding_set(bx, elm_config_scale_get() * 20, 0);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   o = elm_radio_add(bx);
   WEIGHT(o, 0.0, 0.0); FILL(o);
   elm_object_text_set(o, "Custom Color");
   elm_radio_state_value_set(o, 3);
   elm_radio_group_add(o, rdg);
   evas_object_smart_callback_add(o, "changed",
                                  _background_changed_cb, NULL);
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   o = elm_bg_add(bx);
   elm_bg_color_set(o, 255, 255, 255);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); ALIGN(o, 0.0, 0.5);
   evas_object_size_hint_min_set(o, 30, 30);
   evas_object_event_callback_add(o, EVAS_CALLBACK_MOUSE_UP,
                                  _custom_color_mouse_up_cb, NULL);
   elm_box_pack_end(bx, o);
   evas_object_show(o);

   o = elm_radio_add(bx);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0); FILL(o);
   elm_object_text_set(o, "Custom Image");
   elm_radio_state_value_set(o, 4);
   elm_radio_group_add(o, rdg);
   evas_object_smart_callback_add(o, "changed",
                                  _background_changed_cb, NULL);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   elm_radio_value_set(rdg, 1);
}

static void
_finger_size_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                        void *event_info EINA_UNUSED)
{
   double val = elm_slider_value_get(obj);
   double v;

   v = round(val);
   if (v != val) elm_slider_value_set(obj, v);
   option_finger_size_set(v);
   INF("finger size changed : %f %f", val, v);
}

void
option_gui_finger_size_create(Evas_Object *box)
{
   Evas_Object *o;

   o = elm_label_add(box);
   elm_object_text_set(o, "Finger Size");
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   o = elm_slider_add(box);
   elm_slider_unit_format_set(o, "%1.0f");
   elm_slider_indicator_format_set(o, "%1.0f");
   elm_slider_min_max_set(o, 5, 200);
   elm_slider_value_set(o, elm_config_finger_size_get());
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   evas_object_smart_callback_add(o, "delay,changed", _finger_size_changed_cb,
                                  NULL);
   elm_box_pack_end(box, o);
   evas_object_show(o);
}

static void
_scale_changed_cb(void *data EINA_UNUSED, Evas_Object *obj,
                  void *event_info EINA_UNUSED)
{
   double val = elm_slider_value_get(obj);
   double v;

   v = (double)(int)round(val * 10.0) / 10.0;
   if (v != val) elm_slider_value_set(obj, v);

   option_scale_set(v);

   INF("scale changed : %f %f", val, v);
}

void
option_gui_scale_create(Evas_Object *box)
{
   Evas_Object *o;

   ogd->option_scale_prev = option_scale_get();

   o = elm_label_add(box);
   elm_object_text_set(o, "Scale");
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   o = elm_slider_add(box);
   elm_slider_unit_format_set(o, "%1.1f");
   elm_slider_indicator_format_set(o, "%1.1f");
   elm_slider_min_max_set(o, 0.25, 5.0);
   elm_slider_value_set(o, option_scale_get());
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   evas_object_smart_callback_add(o, "delay,changed", _scale_changed_cb, NULL);
   elm_box_pack_end(box, o);
   evas_object_show(o);
}

static Evas_Object *
_option_separator_create(Evas_Object *parent)
{
   Evas_Object *o = NULL;

   o = elm_separator_add(parent);
   elm_separator_horizontal_set(o, EINA_TRUE);
   WEIGHT(o, EVAS_HINT_EXPAND, 0.0);
   FILL(o);
   evas_object_show(o);

   return o;
}

static Evas_Object *
_option_gui_content_create(Evas_Object *parent)
{
   Evas_Object *o = NULL, *frame = NULL, *sc = NULL, *box = NULL, *table = NULL;

   // scroller
   sc = o = elm_scroller_add(parent);
   elm_scroller_bounce_set(o, EINA_FALSE, EINA_TRUE);
   evas_object_show(o);

   // frame
   frame = o = elm_frame_add(sc);
   EXPAND(o); FILL(o); // why?
   elm_object_style_set(o, "pad_large");
   elm_object_content_set(sc, o);
   evas_object_show(o);

   // outer box
   box = o = elm_box_add(sc);
   elm_object_content_set(frame, o);
   evas_object_show(o);

   // finger size
   option_gui_finger_size_create(box);

   elm_box_pack_end(box, _option_separator_create(box));

   // scale
   option_gui_scale_create(box);

   elm_box_pack_end(box, _option_separator_create(box));

   // size
   option_gui_force_resize_create(box);
   option_gui_width_size_create(box);
   option_gui_height_size_create(box);

   elm_box_pack_end(box, _option_separator_create(box));

   // disabled set
   option_gui_disabled_create(box);

   // focus highlight enabled
   option_gui_focus_highlight_create(box);

   // focus highlight animate
   option_gui_focus_highlight_animate_create(box);

   elm_box_pack_end(box, _option_separator_create(box));

   // background change option
   option_gui_background_create(box);

   // padding
   o = elm_box_add(box);
   EXPAND(o); FILL(o);
   elm_box_pack_end(box, o);
   evas_object_show(o);

   table = util_elm_min_set(sc, 300, 200);
   return table;
}

static void
_option_popup_ok_clicked_cb(void *data, Evas_Object *obj EINA_UNUSED,
                            void *event_info EINA_UNUSED)
{
   Evas_Object *popup = data;
   if (!popup) return;

   elm_config_finger_size_set(option_finger_size_get());
   if (gd->preview_obj)
     {
        elm_object_scale_set(gd->preview_obj, option_scale_get());
        widget_resize(gd->preview_obj);
     }

   elm_win_focus_highlight_enabled_set(gd->win, option_focus_highlight_get());
   elm_win_focus_highlight_animate_set(gd->win,
                                       option_focus_highlight_animate_get());

   evas_object_del(popup);
}

static void
_option_popup_save_clicked_cb(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED,
                              void *event_info EINA_UNUSED)
{
   return;
}

static void
_option_popup_cancel_clicked_cb(void *data, Evas_Object *obj EINA_UNUSED,
                                void *event_info EINA_UNUSED)
{
   Evas_Object *popup = data;
   if (!popup) return;

   option_scale_set(ogd->option_scale_prev);
   option_force_resize_set(ogd->option_force_resize_prev);
   option_size_set(ogd->option_size_width_prev, ogd->option_size_height_prev);

   evas_object_del(popup);
}

void
option_gui_create(void)
{
   Evas_Object *popup = NULL, *btn = NULL, *content = NULL;

   if (!ogd)
     {
        ogd = calloc(1, sizeof(Option_Gui_Data));
        ogd->option_size_width_prev = WIDGET_DEFAULT_WIDTH;
        ogd->option_size_height_prev = WIDGET_DEFAULT_HEIGHT;
        ogd->option_scale_prev = 1.0;
     }

   popup = elm_popup_add(gd->win);
   elm_object_part_text_set(popup, "title,text", "Option");

   content = _option_gui_content_create(popup);
   elm_object_content_set(popup, content);

   btn = elm_button_add(popup);
   elm_object_text_set(btn, "Ok");
   elm_object_part_content_set(popup, "button1", btn);
   evas_object_smart_callback_add(btn, "clicked",
                                  _option_popup_ok_clicked_cb, popup);
   evas_object_show(btn);

   btn = elm_button_add(popup);
   elm_object_text_set(btn, "Save");
   elm_object_part_content_set(popup, "button2", btn);
   evas_object_smart_callback_add(btn, "clicked",
                                  _option_popup_save_clicked_cb, popup);
   elm_object_disabled_set(btn, EINA_TRUE);
   evas_object_show(btn);

   btn = elm_button_add(popup);
   elm_object_text_set(btn, "Cancel");
   elm_object_part_content_set(popup, "button3", btn);
   evas_object_smart_callback_add(btn, "clicked",
                                  _option_popup_cancel_clicked_cb, popup);
   evas_object_show(btn);

   evas_object_show(popup);
}
