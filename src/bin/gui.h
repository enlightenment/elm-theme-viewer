#ifndef __INCLUDE_GUI__
#define __INCLUDE_GUI__

#define WIN_WIDTH 700
#define WIN_HEIGHT 600

typedef struct _Gui_Data Gui_Data;
struct _Gui_Data
{
   Evas_Object *win;
   Evas_Object *gui_layout;

   Evas_Object *left_menu_box;
   Eina_Bool left_menu_widget_desc; // true if widget desc is created
   Evas_Object *left_menu_style_genlist; // genlist for the widget styles on the left menu

   Evas_Object *preview_bg;
   Evas_Object *preview_box;
   Evas_Object *preview_obj;
   Evas_Object *widget_naviframe;
   Evas_Object *list;
   Evas_Object *desc_lbl;
   Evas_Object *desc_scr;
   Evas_Object *widget_option_scr; // scroller for widget option
   Eina_Bool m_version;

   // last selected widget and style
   Widget_Type widget;
   const char *style;

   const char *search_widget_name; // search widget name from entry
};

extern Evas_Object *viewer_box;
extern Evas_Object *option_frame;

int gui_init(void);
void gui_shutdown(void);

void gui_create(const char *edje_file,
                Evas_Coord width, Evas_Coord height,
                Eina_Bool fullscreen);

void gui_preview_update(void);
Evas_Object *gui_preview_create(Evas_Object *parent);
void gui_preview_focus(void); // Set the focus to the preview object
Evas_Object *gui_widget_menu_create(Evas_Object *parent);

void gui_version_set(Eina_Bool version);

void gui_panes_clicked_double_cb(void *data, Evas_Object *obj, void *event_info);

Evas_Object * gui_description_label_get(void);

#endif
